import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { css } from '@emotion/core';
import ClipLoader from 'react-spinners/ClipLoader';

export class MessageDialog extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            open: this.props.open ? this.props.open : false
        }
    }
    render() {
        const handleClose = () => {
            this.props.handleDialog(false)
        }
        console.log(this.props)
        const { message, open } = this.props
        return (
            <Dialog
                open={open}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
                maxWidth="md"
            >
                <DialogContent
                    style={{ width: "20rem", overflow: "hidden", background: "none" }}>
                    <DialogContentText id="alert-dialog-description">
                        {message}
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary" autoFocus>
                        Agree
                    </Button>
                </DialogActions>
            </Dialog>
        )
    }
}

export default MessageDialog