import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { css } from '@emotion/core';
import ClipLoader from 'react-spinners/ClipLoader';

export class loadingDialog extends React.Component {
    render() {
        const { open } = this.props
        return (
            <Dialog
                open={open}
                className="custom-loading-dialog"
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
                style={{ background: "none" }}
            >
                <DialogContent
                    style={{ width: "auto", overflow: "hidden", background: "none" }}>
                    <ClipLoader
                        sizeUnit={"px"}
                        size={150}
                        color={'white'}
                        loading={true}
                    />
                </DialogContent>
            </Dialog>
        )
    }
}

export default loadingDialog