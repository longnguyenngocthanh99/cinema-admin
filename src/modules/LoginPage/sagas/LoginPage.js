import { put, takeEvery } from "redux-saga/effects"
import axios from "axios"
import api from "../../../services/api"
import * as actions from "../actions"

const baseUrl = api.urlLogin

export function* handleLogin(action) {
    try {
        let data = {}
        yield axios({
            method: "post",
            data: action.payload,
            url: baseUrl
        }).then((res) => {
            data = { ...res }
        }).catch(e => {
            console.log(e)
        })
        yield put(actions.loginSuccess(data))
    } catch (e) {
        console.log(e)
    }
}

export function* login() {
    yield takeEvery(actions.login, handleLogin)
}

export default {
    login
}