import freeze from "deep-freeze"
import { handleActions } from "redux-actions"
import * as actions from "./actions"

export const name = "LoginPage"

const intialState = freeze({
    data: {
        username: "",
        password: ""
    },
    userInfo: {

    }
})

export default handleActions({
    [actions.loginSuccess]: (state, action) => {
        return freeze({
            ...state,
            userInfo: action.payload.data
        })
    },
    [actions.handleChangeText]: (state, action) => {
        return freeze({
            ...state,
            data: {
                ...state.data,
                [action.payload.name]: action.payload.value
            }
        })
    }
},
    intialState
)