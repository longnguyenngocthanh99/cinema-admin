import React, { Component } from 'react'

export default class LoginPage extends Component {
    render() {
        console.log(this.props)
        const handleLogin = () => {
            this.props.actions.login({
                username: this.props.data.username,
                password: this.props.data.password
            })
        }
        return (
            <div>
                <div className="account-pages"></div>
                <div className="clearfix"></div>
                <div className="wrapper-page">
                    <div className="card-box" style={{ marginTop: "10rem" }}>
                        <div className="text-center">
                            <h4 className="text-uppercase font-bold m-b-0">Sign In</h4>
                        </div>
                        <div className="p-20">
                            <form className="form-horizontal m-t-20" >
                                <div className="form-group">
                                    <div className="col-xs-12">
                                        <input className="form-control"
                                            name="username"
                                            value={this.props.data.username}
                                            onChange={e => this.props.actions.handleChangeText({
                                                name: e.target.name,
                                                value: e.target.value
                                            })}
                                            type="text"
                                            required=""
                                            placeholder="Username" />
                                    </div>
                                </div>

                                <div className="form-group">
                                    <div className="col-xs-12">
                                        <input className="form-control"
                                            value={this.props.data.password}
                                            name="password"
                                            onChange={e => this.props.actions.handleChangeText({
                                                name: e.target.name,
                                                value: e.target.value
                                            })}
                                            type="password"
                                            required=""
                                            placeholder="Password" />
                                    </div>
                                </div>

                                <div className="form-group ">
                                    <div className="col-xs-12">
                                        <div className="checkbox checkbox-custom">
                                            <input id="checkbox-signup" type="checkbox" />
                                            <label for="checkbox-signup">
                                                Remember me
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div className="form-group text-center m-t-30">
                                <div className="col-xs-12">
                                    <button className="btn btn-bordred btn-block waves-effect waves-light"
                                        onClick={e => handleLogin()}
                                        style={{ backgroundColor: "#333", color: "white", border: "0px" }}>
                                        Log In
                                    </button>
                                </div>
                            </div>

                            <div className="form-group m-t-30 m-b-0">
                                <div className="col-sm-12">
                                    <a href="page-recoverpw.html" className="text-muted"><i className="fa fa-lock m-r-5"></i> Forgot your password?</a>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        )
    }
}
