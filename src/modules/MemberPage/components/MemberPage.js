import React, { Component } from 'react'
import MemberTable from "./MemberTable"
import FormMember from "./FormMember"

export default class MemberPage extends Component {
    render() {
        return this.props.isTable ? <MemberTable {...this.props} /> : <FormMember {...this.props} />
    }
}
