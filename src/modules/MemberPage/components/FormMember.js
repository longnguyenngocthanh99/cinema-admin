import React, { Component } from 'react'
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import LoadingDialog from "../../common/loadingDialog"
import MessageDialog from "../../common/messageDialog"
import * as validator from "validator"

export default class FormFilm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            openLoadingDialog: false
        }
    }
    render() {
        const updateFilm = () => {
            const data = this.props.data
            for (let i in data) {
                if (data[i] === "")
                    return this.props.actions.handleFormStatus(false)
            }
            this.props.actions.updateFilm({ data: this.props.data, _id: this.props.data._id })
        }
        const addMember = () => {
            var data = { ...this.props.data }
            data["repassword"] = undefined
            console.log(data)
            for (let i in data) {
                if (data[i] === "")
                    return this.props.actions.handleFormStatus(false)
            }
            if (password !== repassword) {
                return this.props.actions.handleFormStatus(false)
            }
            if (!/^\d+$/.test(phone)) {
                return this.props.actions.handleFormStatus(false)
            }
            console.log(phone.length)
            if (phone.length < 10 || phone.length > 11) {
                return this.props.actions.handleFormStatus(false)
            }
            if (!validator.isEmail(data["email"])) {
                return this.setState({ formStatus: false })
            }
            return this.props.actions.register(this.props.data)
        }
        console.log(this.props)
        const {
            birth,
            address,
            name,
            phone,
            username,
            email,
            password,
            repassword
        } = this.props.data;
        return (
            <div className="row">
                <div className="col-xl-12">
                    <div className="card-box">
                        <h3 className="mb-3">
                            <i className="fa fa-arrow-left" style={{ fontSize: "1.2rem", marginRight: "1rem", cursor: "pointer" }} onClick={e => this.props.actions.handleIsTable(true)}></i>
                            <b>Thông thành viên</b>
                        </h3>

                        <form data-parsley-validate className="row">
                            {this.props.errorStatus ? <div className="col-12 form-group" style={{ height: "2rem", backgroundColor: "#ff81817a", borderRadius: "10px", fontSize: "1rem" }}>
                                {this.props.messageError.map((e, i) => i === 0 ? <span>{e}</span> : <span>, {e}</span>)} đã trùng
                            </div> : ""}
                            <div className="form-group col-md-6">
                                <label htmlFor="userName">Họ tên*</label>
                                <input type="text" name="name" parsley-trigger="change" required
                                    placeholder="Nhập nhập họ tên " className="form-control" id="userName"
                                    value={name}
                                    onChange={e => this.props.actions.handleChangeText(e.target)} />
                                {!this.props.formStatus && name === "" ? <p style={{ color: "red" }}>Hãy nhập họ tên</p> : ""}
                            </div>
                            <div className="form-group col-md-6">
                                <label htmlFor="userName">Tên đăng nhập*</label>
                                <input type="text" name="username" parsley-trigger="change" required
                                    placeholder="Nhập tên đăng nhập" className="form-control" id="userName"
                                    value={username}
                                    onChange={e => this.props.actions.handleChangeText(e.target)} />
                                {!this.props.formStatus ? (username === "" ? <p style={{ color: "red" }}>Vui lòng nhập Tên đăng nhập</p> : (username.length < 8 ? <p style={{ color: "red" }}>Tên đăng nhập phải từ 8 kí tự</p> : "")) : ""}
                            </div>
                            <div className="form-group col-md-6">
                                <label htmlFor="userName">Email*</label>
                                <input type="text" name="email" parsley-trigger="change" required
                                    placeholder="Nhập Email" className="form-control" id="userName"
                                    value={email}
                                    onChange={e => this.props.actions.handleChangeText(e.target)} />
                                {!this.props.formStatus ? (email === "" ? <p style={{ color: "red" }}>Vui lòng nhập địa chỉ email</p> : (!validator.isEmail(email) ? <p style={{ color: "red" }}>Email bạn nhập không chính xác</p> : "")) : ""}
                            </div>

                            <div className="form-group col-6">
                                <label htmlFor="userName">Số điện thoại*</label>
                                <input type="text" name="phone" parsley-trigger="change" required
                                    placeholder="Nhập số điện thoại" className="form-control" id="userName"
                                    value={phone}
                                    onChange={e => this.props.actions.handleChangeText(e.target)} />
                                {!this.props.formStatus ? (phone === "" || (phone.length < 10 || phone.length > 11) ?
                                    <p style={{ color: "red" }}> Số điện thoại phải từ 10 đến 11 số</p> :
                                    (!/^\d+$/.test(phone) ? <p style={{ color: "red" }}>Số điện thoại chỉ có số</p> : "")) : ""}
                            </div>

                            <div className="form-group col-6">
                                <label htmlFor="userName">Password *</label>
                                <input type="text" name="password" parsley-trigger="change" required
                                    placeholder="Nhập password" className="form-control" id="userName"
                                    value={password}
                                    onChange={e => this.props.actions.handleChangeText(e.target)} />
                                {!this.props.formStatus && password === "" ? <p style={{ color: "red" }}>Hãy nhập password</p> : ""}
                            </div>

                            <div className="form-group col-md-6">
                                <label htmlFor="userName">Nhập lại Password*</label>
                                <input type="text" name="repassword" parsley-trigger="change" required
                                    placeholder="Nhập lại Password" className="form-control" id="userName"
                                    value={repassword}
                                    onChange={e => this.props.actions.handleChangeText(e.target)} />
                                {!this.props.formStatus ? (repassword === "" ? <p style={{ color: "red" }}>Vui lòng nhập xác nhận password</p> : (password !== repassword ? <p style={{ color: "red" }}>Password và xác nhận password không chính xác</p> : "")) : ""}
                            </div>

                            <div className="form-group col-6">
                                <label htmlFor="userName">Địa chỉ*</label>
                                <input type="text" name="address" parsley-trigger="change" required
                                    placeholder="Nhập địa chỉ" className="form-control" id="userName"
                                    value={address}
                                    onChange={e => this.props.actions.handleChangeText(e.target)} />
                                {!this.props.formStatus && address === "" ? <p style={{ color: "red" }}>Xin vui lòng nhập địa chỉ</p> : ""}
                            </div>

                            <div className="form-group col-6">
                                <label htmlFor="userName">Ngày sinh*</label>
                                <br />
                                <DatePicker
                                    selected={this.props.dateForm}
                                    onChange={e => this.props.actions.handleChangeDate(e)}
                                    dateFormat="dd/MM/yyyy"
                                    peekNextMonth
                                    showMonthDropdown
                                    showYearDropdown
                                    dropdownMode="select"
                                />
                                {!this.props.formStatus && birth === "" ? <p style={{ color: "red" }}>Hãy chọn ngày sinh</p> : ""}
                            </div>

                        </form>

                        <div className="form-group text-right m-b-0">
                            <button className="btn btn-primary waves-effect waves-light" onClick={e => {
                                return this.props.data._id ?
                                    updateFilm() :
                                    addMember()
                            }}>
                                Submit
                            </button>
                            <button className="btn btn-secondary waves-effect waves-light m-l-5" onClick={e => {
                                this.props.actions.handleIsTable(true)
                            }}>
                                Cancel
                            </button>
                        </div>
                        <LoadingDialog open={this.state.openLoadingDialog} />
                        <MessageDialog message={this.props.errorStatus ? "Save fail" : "Save success"} handleDialog={this.props.actions.handleDialogMessage} open={this.props.openMessageDialog} />
                    </div>
                </div>

            </div >
        )
    }
}
