import { createAction } from "redux-actions"
import * as CONST from "./constants"

export const getMember = createAction(CONST.GET_MEMBER)
export const getMemberSuccess = createAction(CONST.GET_MEMBER_SUCCESS)
export const getMemberFail = createAction(CONST.GET_MEMBER_FAIL)

export const register = createAction(CONST.REGISTER)
export const registerSuccess = createAction(CONST.REGISTERSUCCESS)
export const registerFail = createAction(CONST.REGISTERFAIL)

export const handleInitData = createAction(CONST.HANDLE_INIT_DATA)
export const handleDialogMessage = createAction(CONST.HANDLE_DIALOG_MESSAGE)
export const handleChangeText = createAction(CONST.HANDLECHANGETEXT)
export const handleChangeDate = createAction(CONST.HANDLECHANGEDATE)
export const handleIsTable = createAction(CONST.HANDLE_IS_TABLE_FILM)
export const handleGetInfoFilm = createAction(CONST.HANDLE_GET_INFO_FILM)
export const handleFormStatus = createAction(CONST.HANDLE_FORM_STATUS)