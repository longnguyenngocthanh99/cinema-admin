import freeze from "deep-freeze"
import { handleActions } from "redux-actions"
import * as actions from "./actions"

export const name = "MemberPage"

const intialState = freeze({
    members: [],
    data: {
        username: "",
        password: "",
        name: "",
        address: "",
        phone: "",
        birth: "",
        email: "",
        repassword: ""
    },
    dateForm: new Date,
    imgShow1: "",
    imgShow2: "",
    isTable: true,
    openLoadingDialog: false,
    openMessageDialog: false,
    formStatus: true,
    errorStatus: false,
    messageError: []
})

export default handleActions({
    [actions.handleFormStatus]: (state, action) => {
        return freeze({
            ...state,
            formStatus: action.payload
        })
    },
    [actions.handleChangeText]: (state, action) => {
        return freeze({
            ...state,
            data: {
                ...state.data,
                [action.payload.name]: action.payload.value
            }
        })
    },
    [actions.handleDialogMessage]: (state, action) => {
        return freeze({
            ...state,
            openMessageDialog: action.payload,
            isTable: !action.payload ? true : false
        })
    },
    [actions.handleChangeDate]: (state, action) => {
        console.log(action.payload)
        const date = action.payload
        return freeze({
            ...state,
            data: {
                ...state.data,
                birth: date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear()
            },
            dateForm: date
        })
    },
    [actions.getMemberSuccess]: (state, action) => {
        return freeze({
            ...state,
            members: action.payload.members
        })
    },
    [actions.handleIsTable]: (state, action) => {
        return freeze({
            ...state,
            isTable: action.payload
        })
    },
    [actions.handleInitData]: (state, action) => {
        return freeze({
            ...state,
            data: intialState.data,
            imgShow1: "",
            imgShow2: ""
        })
    },
    [actions.registerSuccess]: (state, action) => {
        console.log(action.payload)
        return freeze({
            ...state,
            errorStatus: false,
            messageError: [],
            data: intialState.data,
            openLoadingDialog: false,
            openMessageDialog: true,
            formStatus: true,
        })
    },
    [actions.registerFail]: (state, action) => {
        return freeze({
            ...state,
            errorStatus: true,
            messageError: action.payload.response.data,
            userInfo: "",
            openLoadingDialog: false,
            openMessageDialog: true,
            formStatus: true
        })
    },
},
    intialState
)