import { put, takeEvery } from "redux-saga/effects"
import axios from "axios"
import api from "../../../services/api"
import * as actions from "../actions"

const baseUrl = api.urlMember
const urlQuestRegister = api.urlQuestRegister

export function* handleGetMember() {
    try {
        let data = {}
        yield axios({
            method: "get",
            url: baseUrl,
        }).then((res) => {
            console.log(res)
            data.members = res.data
        }).catch(e => {
            console.log(e)
        })
        yield put(actions.getMemberSuccess(data))
    } catch (e) {
        console.log(e)
    }
}

export function* getMember() {
    yield takeEvery(actions.getMember, handleGetMember)
}

export function* handleRegister(action) {
    try {
        let data = {}
        let error = ""
        console.log(action.payload)
        yield axios({
            method: "post",
            data: action.payload,
            url: urlQuestRegister
        }).then(res => {
            console.log(res)
            console.log(res.data)
            data = res.data
            console.log(data)
        }).catch(err => {
            error = err
        })
        console.log(data)
        if (error) {
            return yield put(actions.registerFail(error))
        }
        console.log(data)
        yield put(actions.registerSuccess(data))
    } catch (err) {
        console.log(err)
    }
}

export function* register() {
    yield takeEvery(actions.register, handleRegister)
}

export default {
    getMember,
    register
}