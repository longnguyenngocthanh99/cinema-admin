import React, { Component } from 'react'
import ShowtimeTable from "./ShowtimeTable"
import ShowtimeForm from "./ShowtimeForm"

export default class ShowtimePage extends Component {
    render() {
        return this.props.isTable ? <ShowtimeTable {...this.props} /> : <ShowtimeForm {...this.props} />
    }
}
