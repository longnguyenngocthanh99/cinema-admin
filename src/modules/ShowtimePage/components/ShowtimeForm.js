import React, { Component } from 'react'
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import LoadingDialog from "../../common/loadingDialog"
import MessageDialog from "../../common/messageDialog"
import * as validator from "validator"

export default class ShowtimeForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            openLoadingDialog: false,
            message: "Save success"
        }
    }
    render() {
        const updateShowtime = () => {
            if (this.props.data.showtime.tickets) {
                this.setState({
                    message: "Không thể thay đổi vì đã có khách đặt ghế"
                })
                return this.props.actions.handleDialogMessage(true)
            }
            const data = this.props.data.showtime.status
            this.props.actions.updateShowtime({ data: { status: data }, _id: this.props.data.showtime._id })
        }
        const submitShowtime = () => {
            if (this.props.data.showtime._id) {
                return updateShowtime()
            }
            var data = { ...this.props.data }
            var showtimes = [...this.props.showtimes]
            for (let i in showtimes) {
                if (showtimes[i].timeShow === data.timeShow)
                    return this.props.actions.handleFormStatus(false)
            }
            console.log(data)
            return this.props.actions.addShowtime(this.props.data)
        }
        console.log(this.props)
        const {
            cinema,
            room,
            film,
        } = this.props;

        const alpha = [
            "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "Q", "Z", "Y", "Z"
        ]
        const mapSeat = this.props.data.showtime.map ? this.props.data.showtime.map : []
        const displayAlpha = (alphabet, mapSeat) => {
            if (JSON.stringify(mapSeat) === JSON.stringify([]))
                return ""
            return mapSeat.map((seats, index) =>
                <div style={{ width: "10%", height: "4rem" }} key={index}>
                    <button style={{ background: "white", border: "1px solid black", outline: "none", width: "1.5rem", height: "3rem" }}>{alphabet[index]}</button>
                </div>
            )
        }
        const displaySeats = (alphabet, mapSeat) => {
            if (JSON.stringify(mapSeat) === JSON.stringify([]))
                return ""
            return mapSeat.map((seats, indexAlpha) => seats.map((seat, index) => {
                return <div style={{ width: "10%", height: "4rem" }} key={index}>
                    <button style={{ background: seat === 0 ? "#dbdee1" : seat === 1 ? "#28a745" : "red", color: seat === 0 ? "black" : "white", border: "none", outline: "none", width: "80%", height: "3rem", cursor: "pointer" }} disabled={seat === 1}>{index % 10 + 1}</button>
                </div>
            }))
        }
        return (
            <div className="row">
                <div className="col-xl-12" style={{ marginBottom: "15rem", padding: "0px 1rem" }}>
                    <div className="card-box">
                        <h3 className="mb-3">
                            <i className="fa fa-arrow-left" style={{ fontSize: "1.2rem", marginRight: "1rem", cursor: "pointer" }} onClick={e => this.props.actions.handleIsTable(true)}></i>
                            <b>Thông tin suất chiếu</b>
                        </h3>

                        <form data-parsley-validate className="row mb-5">
                            <div className="col-xl-3 col-12 mt-2" style={{ height: "5rem" }}>
                                <label htmlFor="userName">Chọn ngày*</label>
                                <DatePicker
                                    selected={this.props.dateForm}
                                    disabled={this.props.data.showtime._id}
                                    onChange={async (e) => {
                                        await this.props.actions.handleChangeDate(e)
                                    }}
                                    dateFormat="dd/MM/yyyy"
                                    peekNextMonth
                                    showMonthDropdown
                                    showYearDropdown
                                    dropdownMode="select"
                                />
                            </div>
                            <div className="col-xl-3 col-12 mt-2" style={{ height: "5rem" }}>
                                <label htmlFor="userName">Chọn thời gian*</label>
                                <DatePicker
                                    // selected={startDate}
                                    selected={this.props.timeShow}
                                    onChange={(e) => {
                                        this.props.actions.handleChangeTime(e)
                                    }}
                                    showTimeSelect
                                    showTimeSelectOnly
                                    timeIntervals={15}
                                    timeCaption="Time"
                                    dateFormat="h:mm aa"
                                />
                                {!this.props.formStatus ? <p style={{ color: "red" }}>Thời gian đã bị trùng</p> : ""}
                            </div>
                            <div class="form-group col-xl-3 col-12" style={{ height: "5rem" }}>
                                <label class="col-form-label">Chọn rạp</label>
                                <div>
                                    <select class="form-control" name="cinema"
                                        disabled={this.props.data.showtime._id}
                                        defaultValue={JSON.stringify(this.props.data.cinema)}
                                        onChange={async (e) => {
                                            const data = JSON.parse(e.target.value)
                                            const name = e.target.name
                                            await this.props.actions.handleChangeText({ value: data, name: name })
                                            this.props.actions.getRoom(data._id)
                                        }}>
                                        {cinema.map((e, i) => {
                                            return <option key={e._id} value={JSON.stringify(e)}>{e.name}</option>
                                        })}
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-xl-3 col-12" style={{ height: "5rem" }}>
                                <label class="col-form-label">Chọn phòng</label>
                                <div>
                                    <select class="form-control" name="room"
                                        disabled={this.props.data.showtime._id}
                                        defaultValue={JSON.stringify(this.props.data.room)}
                                        onChange={async (e) => {
                                            const data = JSON.parse(e.target.value)
                                            const name = e.target.name
                                            await this.props.actions.handleChangeText({ value: data, name: name })
                                        }}>
                                        {room.map(e => {
                                            return <option key={e._id} value={JSON.stringify(e)}>{e.name}</option>
                                        })}
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-xl-3 col-12" style={{ height: "5rem" }}>
                                <label class="col-form-label">Chọn phim</label>
                                <div>
                                    <select class="form-control" name="film"
                                        disabled={this.props.data.showtime._id}
                                        defaultValue={JSON.stringify(this.props.data.film)}
                                        onChange={async (e) => {
                                            const data = JSON.parse(e.target.value)
                                            const name = e.target.name
                                            await this.props.actions.handleChangeText({ value: data, name: name })
                                        }}>
                                        {film.map(e => <option key={e._id} value={JSON.stringify(e)}>{e.name}</option>)}
                                    </select>
                                </div>
                            </div>
                            {this.props.data.showtime._id ?
                                <div class="form-group col-xl-3 col-12" style={{ height: "5rem" }}>
                                    <label class="col-form-label">Trạng thái</label>
                                    <div>
                                        <select class="form-control"
                                            defaultValue={this.props.data.showtime.status}
                                            name="status"
                                            onChange={async (e) => {
                                                const data = JSON.parse(e.target.value)
                                                await this.props.actions.handleChangeStatus(data)
                                            }}>
                                            <option value={true}>Sẵn sàn</option>
                                            <option value={false}>Hoãn</option>
                                        </select>
                                    </div>
                                </div> : ""
                            }
                        </form>

                        {this.props.data.showtime._id ?
                            <div>
                                <div className="row">
                                    <div className="col-lg-3 col-12">
                                        <p style={{ fontSize: "1.5rem", color: "red", fontWeight: "bold" }}>Sơ đồ chỗ ngồi :</p>
                                    </div>
                                    <div className="row col-12 col-lg-6 mb-3">
                                        <div className="col">
                                            <span style={{ fontSize: "1rem" }}>Ghế trống : </span>
                                            <button style={{ background: "#dbdee1", width: "1rem", height: "1rem", border: "none", outline: "none" }}></button>
                                        </div>
                                        <div className="col">
                                            <span style={{ fontSize: "1rem" }}>Ghế đã chọn : </span>
                                            <button style={{ background: "red", width: "1rem", height: "1rem", border: "none", outline: "none" }}></button>
                                        </div>
                                    </div>
                                    <div className="col-lg-3 col-0"></div>
                                    <div className="col-2 col-lg-3"></div>
                                    <div className="col-9 col-lg-6" style={{ background: "black", height: "1rem" }}>

                                    </div>
                                    <div className="col-1 col-lg-3"></div>
                                    <div className="col-12" style={{ textAlign: "center" }}>
                                        <p style={{ fontSize: "1.2rem" }}>Màn hình</p>
                                    </div>
                                    <div className="container col-12 col-lg-12 row">
                                        <div className="col-1 col-lg-2">

                                        </div>
                                        <div className="col-1" style={{}}>
                                            {displayAlpha(alpha, mapSeat)}
                                        </div>
                                        <div className="row col-10 col-lg-6" style={{ width: "100%", textAlign: 'center', margin: "0px" }}>
                                            {displaySeats(alpha, mapSeat)}
                                        </div>
                                        <div style={{ width: "100%", height: "2rem" }}></div>
                                    </div>
                                </div>
                                <div className="row p-3" style={{ textAlign: "center" }}>
                                    <div className="col-4 row">
                                        <div className="col-12">
                                            <p style={{ fontSize: "1.2rem" }}>Thông tin vé :</p>
                                        </div>
                                        <div className="col-12">
                                            {this.props.data.showtime.detailTickets.map(e => {
                                                return <p>{e.name} : {e.price}đ ({e.count})</p>
                                            })}
                                            <p style={{ fontSize: "1.2rem" }}>Tổng : {this.props.data.showtime.revenueTickets} Đ</p>
                                        </div>
                                    </div>
                                    <div className=" col-4 row">
                                        <div className="col-12">
                                            <p style={{ fontSize: "1.2rem" }}>Thông tin combo :</p>
                                            {this.props.data.showtime.combo.map(e => {
                                                return <p>{e.name} : {e.price}đ ({e.count})</p>
                                            })}
                                            <p style={{ fontSize: "1.2rem" }}>Tổng : {this.props.data.showtime.revenueCombo} Đ</p>
                                        </div>
                                    </div>
                                    <div className=" col-4">
                                        <p style={{ fontSize: "1.5rem", color: "red" }}>Doanh thu : {this.props.data.showtime.revenue} Đ</p>
                                    </div>

                                </div>
                            </div>
                            : ""}

                        <div className="form-group text-right m-b-0">
                            <button className="btn btn-primary waves-effect waves-light" onClick={e => {
                                return submitShowtime()
                            }}>
                                Submit
                            </button>
                            <button className="btn btn-secondary waves-effect waves-light m-l-5" onClick={e => {
                                this.props.actions.handleIsTable(true)
                            }}>
                                Cancel
                            </button>
                        </div>
                        <LoadingDialog open={this.state.openLoadingDialog} />
                        <MessageDialog message={this.props.errorStatus ? "Save fail" : this.state.message} handleDialog={this.props.actions.handleDialogMessage} open={this.props.openMessageDialog} />
                    </div>
                </div>

            </div >
        )
    }
}
