import React, { Component } from 'react'
import Paper from "@material-ui/core/Paper";
import // State or Local Processing Plugins
    "@devexpress/dx-react-grid";
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

export default class ShowtimeTable extends Component {
    constructor(props) {
        super(props);

        this.state = {
            pageSizes: 5,
            columns: [
                { name: "name", title: "Tên" },
                { name: "phone", title: "Điện thoại" },
                { name: "email", title: "Email" },
                { name: "level", title: "Level" },
                { name: "point", title: "Điểm" },
            ],
            rows: this.props.movies ? this.props.movies : [],
            selection: []
        };
    }
    componentDidMount() {
        console.log(this.props.data.date.split("/").reverse().join("/"))
        this.props.actions.getData(this.props.data.date.split("/").reverse().join("/"))
        // this.props.actions.getRoom(this.props.data.cinema._id)
    }
    render() {
        console.log(this.props)
        const { cinema, film, room, showtimes, dateForm } = this.props
        return (
            <div className=" card">
                <div className=" row mt-3" style={{ marginBottom: "15rem", padding: "0px 1rem" }}>
                    <div className=" col-12">
                        <button
                            type="button"
                            class="btn btn-danger waves-effect w-md waves-light col-xl-2 col-lg-3 col-5"
                            style={{ zIndex: "0" }}
                            onClick={e =>
                                this.props.actions.handleIsTable(false)}>
                            Thêm xuất chiếu <i className="fa fa-plus ml-2" />
                        </button>
                    </div>
                    <div className="col-xl-3 col-12 mt-2" style={{ height: "5rem" }}>
                        <label htmlFor="userName">Chọn ngày*</label>
                        <DatePicker
                            selected={this.props.dateForm}
                            onChange={async (e) => {
                                await this.props.actions.handleChangeDate(e)
                                this.props.actions.getShowtime({
                                    idCinema: this.props.data.cinema._id,
                                    idFilm: this.props.data.film._id,
                                    idTheater: this.props.data.room._id,
                                    dayShow: this.props.data.date.split("/").reverse().join("/"),
                                })
                            }}
                            dateFormat="dd/MM/yyyy"
                            peekNextMonth
                            showMonthDropdown
                            showYearDropdown
                            dropdownMode="select"
                        />
                    </div>
                    <div class="form-group col-xl-3 col-12" style={{ height: "5rem" }}>
                        <label class="col-form-label">Chọn rạp</label>
                        <div>
                            <select class="form-control" name="cinema" onChange={async (e) => {
                                const data = JSON.parse(e.target.value)
                                const name = e.target.name
                                await this.props.actions.handleChangeText({ value: data, name: name })
                                console.log(data)
                                await this.props.actions.getRoom(data._id)
                                await this.props.actions.getShowtime({
                                    idCinema: this.props.data.cinema._id,
                                    idFilm: this.props.data.film._id,
                                    dayShow: this.props.data.date.split("/").reverse().join("/"),
                                })
                            }}>
                                {cinema.map((e, i) => {
                                    return <option key={e._id} value={JSON.stringify(e)}>{e.name}</option>
                                })}
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-xl-3 col-12" style={{ height: "5rem" }}>
                        <label class="col-form-label">Chọn phòng</label>
                        <div>
                            <select class="form-control" name="room" onChange={async (e) => {
                                const data = JSON.parse(e.target.value)
                                const name = e.target.name
                                console.log(data)
                                await this.props.actions.handleChangeText({ value: data, name: name })
                                this.props.actions.getShowtime({
                                    idCinema: this.props.data.cinema._id,
                                    idFilm: this.props.data.film._id,
                                    idTheater: this.props.data.room._id,
                                    dayShow: this.props.data.date.split("/").reverse().join("/"),
                                })
                            }}>
                                {room.map(e => <option key={e._id} value={JSON.stringify(e)}>{e.name}</option>)}
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-xl-3 col-12" style={{ height: "5rem" }}>
                        <label class="col-form-label">Chọn phim</label>
                        <div>
                            <select class="form-control" name="film" onChange={async (e) => {
                                const data = JSON.parse(e.target.value)
                                const name = e.target.name
                                await this.props.actions.handleChangeText({ value: data, name: name })
                                this.props.actions.getShowtime({
                                    idCinema: this.props.data.cinema._id,
                                    idFilm: this.props.data.film._id,
                                    idTheater: this.props.data.room._id,
                                    dayShow: this.props.data.date.split("/").reverse().join("/"),
                                })
                            }}>
                                {film.map(e => <option key={e._id} value={JSON.stringify(e)}>{e.name}</option>)}
                            </select>
                        </div>
                    </div>
                    <div className="col-12" style={{ textAlign: "center" }}>
                        <h3>Danh sách xuất chiếu</h3>
                    </div>
                    <div className="row col-12 mb-3" style={{ textAlign: "center" }}>
                        <div className="col-6" style={{ textAlign: "right" }}>
                            <span style={{ fontSize: "1rem" }}>Xuất sẵn sàn : </span>
                            <button style={{ background: "#188ae2", width: "1rem", height: "1rem", border: "none", outline: "none" }}></button>
                        </div>
                        <div className="col-6" style={{ textAlign: "left" }}>
                            <span style={{ fontSize: "1rem" }}>Xuất hoãn : </span>
                            <button style={{ background: "#ff5b5b", width: "1rem", height: "1rem", border: "none", outline: "none" }}></button>
                        </div>
                    </div>
                    <div className="col-2"></div>
                    {
                        showtimes.length === 0 ?
                            <div className="col-8" style={{ textAlign: "center", fontWeight: "bold", color: "red" }}>
                                <h5 style={{ textAlign: "center", }}>Không có suất chiếu</h5>
                            </div> :
                            <div className="form-group col-8 row">
                                {showtimes.map(e =>
                                    <div className="col-xl-3 col-lg-4 col-6 mt-3" style={{ textAlign: "center" }}>
                                        <button type="button" class={e.status ? "btn btn-primary waves-effect w-md waves-light m-b-5" : "btn btn-danger waves-effect w-md waves-light m-b-5"} style={{ zIndex: "0", width: "80%" }} onClick={() => {
                                            // console.log(e)
                                            this.props.actions.handleShowtimeDetail(e)
                                            this.props.actions.handleIsTable(false)
                                            this.props.actions.handleChangeTime(new Date(
                                                dateForm.getFullYear(),
                                                dateForm.getMonth(),
                                                dateForm.getDate(),
                                                e.timeShow.substring(0, 2),
                                                e.timeShow.substring(3, 5)
                                            ))
                                        }}>
                                            {e.timeShow}
                                        </button>
                                        <p style={{ fontSize: "0.8rem", textAlign: "center", color: "red", fontWeight: "bold" }}>
                                            {e.tickets + "/" + this.props.data.room.seatNumber}
                                        </p>
                                    </div>
                                )}
                            </div>

                    }
                </div>
            </div >
        )
    }
}
