import React from "react"
import { bindActionCreators } from "redux"
import { connect } from "react-redux"
import { withRouter } from "react-router-dom"
import { name } from "../reducers"
import * as action from "../actions"
import ShowtimePage from "./ShowtimePage"

class ShowtimePageContainer extends React.Component {
    componentDidMount() {
        this.props.actions.handleIsTable(true)
    }
    render() {
        return (
            <React.Fragment>
                <ShowtimePage {...this.props} />
            </React.Fragment>
        )
    }
}

function mapStateToProps(state) {
    return {
        ...state[name]
    }
}

function mapDispatchToProps(dispatch) {
    const actions = {
        ...action,
    }
    return { actions: bindActionCreators(actions, dispatch) }
}

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(ShowtimePageContainer)
)