import freeze from "deep-freeze"
import { handleActions } from "redux-actions"
import * as actions from "./actions"

export const name = "ShowtimePage"
const newDate = new Date(2019, 10, 30)

const intialState = freeze({
    showtimes: [],
    cinema: [],
    film: [],
    room: [],
    data: {
        cinema: "",
        film: "",
        date: newDate.getDate() + "/" + (newDate.getMonth()) + "/" + newDate.getFullYear(),
        room: "",
        timeShow: "12:30",
        showtime: {}
    },
    timeShow: new Date(2019, 9, 30, 12, 30),
    dateForm: new Date(2019, 9, 30),
    imgShow1: "",
    imgShow2: "",
    isTable: true,
    openLoadingDialog: false,
    openMessageDialog: false,
    formStatus: true,
    errorStatus: false,
    messageError: []
})

export default handleActions({
    [actions.handleFormStatus]: (state, action) => {
        return freeze({
            ...state,
            formStatus: action.payload
        })
    },
    [actions.handleChangeText]: (state, action) => {
        return freeze({
            ...state,
            data: {
                ...state.data,
                [action.payload.name]: action.payload.value
            }
        })
    },
    [actions.handleChangeDate]: (state, action) => {
        console.log(action.payload)
        const date = action.payload
        return freeze({
            ...state,
            data: {
                ...state.data,
                date: date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear()
            },
            dateForm: date
        })
    },
    [actions.handleChangeStatus]: (state, action) => {
        console.log(action.payload)
        return freeze({
            ...state,
            data: {
                ...state.data,
                showtime: {
                    ...state.data.showtime,
                    status: action.payload
                }
            },
        })
    },
    [actions.handleChangeTime]: (state, action) => {
        console.log(action.payload)
        const date = action.payload
        return freeze({
            ...state,
            data: {
                ...state.data,
                timeShow: date.getHours() + ":" + (date.getMinutes() !== 0 ? date.getMinutes() : "00")
            },
            timeShow: date
        })
    },
    [actions.handleDialogMessage]: (state, action) => {
        return freeze({
            ...state,
            openMessageDialog: action.payload,
            isTable: (!action.payload && !state.data.showtime._id) ? true : false,
        })
    },
    [actions.getDataSuccess]: (state, action) => {
        let temp = action.payload.showtimes.sort((a, b) => {
            return a['timeShow'].localeCompare(b['timeShow'])
        })
        console.log(temp)
        return freeze({
            ...state,
            cinema: action.payload.cinema,
            film: action.payload.film,
            room: action.payload.theater,
            showtimes: temp,
            data: {
                ...state.data,
                cinema: action.payload.cinema[0],
                film: action.payload.film[0],
                room: action.payload.theater[0]
            },
        })
    },
    [actions.getRoomSuccess]: (state, action) => {
        return freeze({
            ...state,
            room: action.payload,
            data: {
                ...state.data,
                room: action.payload[0]
            },
        })
    },
    [actions.updateShowtimeSuccess]: (state, action) => {
        return freeze({
            ...state,
            openLoadingDialog: false,
            openMessageDialog: true,
            formStatus: true,
        })
    },
    [actions.getShowtimeSuccess]: (state, action) => {
        let temp = action.payload.sort((a, b) => {
            return a['timeShow'].localeCompare(b['timeShow'])
        })
        return freeze({
            ...state,
            showtimes: temp,
        })
    },
    [actions.handleIsTable]: (state, action) => {
        return freeze({
            ...state,
            isTable: action.payload,
            data: {
                ...state.data,
                showtime: action.payload ? {} : state.data.showtime
            }
        })
    },
    [actions.handleShowtimeDetail]: (state, action) => {
        return freeze({
            ...state,
            data: {
                ...state.data,
                showtime: action.payload
            }
        })
    },
    [actions.handleInitData]: (state, action) => {
        return freeze({
            ...state,
            data: intialState.data,
            imgShow1: "",
            imgShow2: ""
        })
    },
    [actions.addShowtimeSuccess]: (state, action) => {
        console.log(action.payload)
        return freeze({
            ...state,
            errorStatus: false,
            messageError: [],
            data: intialState.data,
            openLoadingDialog: false,
            openMessageDialog: true,
            formStatus: true,
        })
    },
},
    intialState
)