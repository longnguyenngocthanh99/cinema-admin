import { createAction } from "redux-actions"
import * as CONST from "./constants"

export const getData = createAction(CONST.GET_DATA)
export const getDataSuccess = createAction(CONST.GET_DATA_SUCCESS)
export const getDataSuccessFail = createAction(CONST.GET_DATA_FAIL)

export const getRoom = createAction(CONST.GET_ROOM)
export const getRoomSuccess = createAction(CONST.GET_ROOM_SUCCESS)
export const getRoomSuccessFail = createAction(CONST.GET_ROOM_FAIL)

export const getShowtime = createAction(CONST.GET_SHOWTIME)
export const getShowtimeSuccess = createAction(CONST.GET_SHOWTIME_SUCCESS)
export const getShowtimeSuccessFail = createAction(CONST.GET_SHOWTIME_FAIL)

export const addShowtime = createAction(CONST.ADD_SHOWTIME)
export const addShowtimeSuccess = createAction(CONST.ADD_SHOWTIME_SUCCESS)
export const addShowtimeFail = createAction(CONST.ADD_SHOWTIME_FAIL)

export const updateShowtime = createAction(CONST.UPDATE_SHOWTIME)
export const updateShowtimeSuccess = createAction(CONST.UPDATE_SHOWTIME_SUCCESS)
export const updateShowtimeFail = createAction(CONST.UPDATE_SHOWTIME_FAIL)

export const handleInitData = createAction(CONST.HANDLE_INIT_DATA)
export const handleShowtimeDetail = createAction(CONST.HANDLE_SHOWTIME_DETAIL)
export const handleDialogMessage = createAction(CONST.HANDLE_DIALOG_MESSAGE)
export const handleChangeText = createAction(CONST.HANDLECHANGETEXT)
export const handleChangeDate = createAction(CONST.HANDLECHANGEDATE)
export const handleChangeTime = createAction(CONST.HANDLE_CHANGE_TIME)
export const handleIsTable = createAction(CONST.HANDLE_IS_TABLE_FILM)
export const handleGetInfoFilm = createAction(CONST.HANDLE_GET_INFO_FILM)
export const handleFormStatus = createAction(CONST.HANDLE_FORM_STATUS)


export const handleChangeStatus = createAction(CONST.HANDLE_CHANGE_STATUS)