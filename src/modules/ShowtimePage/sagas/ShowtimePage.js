import { put, takeEvery } from "redux-saga/effects"
import axios from "axios"
import api from "../../../services/api"
import * as actions from "../actions"

const baseUrl = api.urlShowtimes
const urlTheaterByCinema = api.urlTheaterByCinema
const urlQuestRegister = api.urlQuestRegister
const urlShowtimesGeneral = api.urlShowtimesGeneral
const urlShowtimesInit = api.urlShowtimesInit
export function* handleGetdata(action) {
    try {
        let data = {}
        console.log(action.payload)
        yield axios({
            method: "post",
            url: urlShowtimesInit,
            data: { dayShow: action.payload }
        }).then((res) => {
            console.log(res)
            data = res.data
        }).catch(e => {
            console.log(e)
        })
        yield put(actions.getDataSuccess(data))
    } catch (e) {
        console.log(e)
    }
}

export function* getdata() {
    yield takeEvery(actions.getData, handleGetdata)
}

export function* handleGetRoom(action) {
    try {
        let data = {}
        console.log(urlTheaterByCinema + action.payload)
        yield axios({
            method: "get",
            url: urlTheaterByCinema + action.payload,
        }).then((res) => {
            console.log(res)
            data = res.data
        }).catch(e => {
            console.log(e)
        })
        yield put(actions.getRoomSuccess(data))
    } catch (e) {
        console.log(e)
    }
}

export function* getRoom() {
    yield takeEvery(actions.getRoom, handleGetRoom)
}

export function* handleGetShowtime(action) {
    try {
        let data = {}
        console.log(urlShowtimesGeneral)
        yield axios({
            method: "post",
            url: urlShowtimesGeneral,
            data: action.payload
        }).then((res) => {
            console.log(res)
            data = res.data
        }).catch(e => {
            console.log(e)
        })
        yield put(actions.getShowtimeSuccess(data))
    } catch (e) {
        console.log(e)
    }
}

export function* getShowtime() {
    yield takeEvery(actions.getShowtime, handleGetShowtime)
}

export function* handleAddShowtime(action) {
    try {
        let data = {}
        let map = []
        for (let i = 0; i < action.payload.room.seatNumber; i++) {
            if (!(i % 10)) {
                map.push([])
                map[parseInt(i / 10)].push(0)
            } else {
                map[parseInt(i / 10)].push(0)
            }
        }
        console.log(map)
        let error = ""
        console.log(action.payload)
        yield axios({
            method: "post",
            data: {
                idFilm: action.payload.film._id,
                idCinema: action.payload.cinema._id,
                idTheater: action.payload.room._id,
                dayShow: action.payload.date.split("/").reverse().join("/"),
                timeShow: action.payload.timeShow,
                map: map
            },
            url: baseUrl
        }).then(res => {
            data = res.data
        }).catch(err => {
            error = err
        })
        console.log(data)
        if (error) {
            return yield put(actions.addShowtimeFail(error))
        }
        console.log(data)
        yield put(actions.addShowtimeSuccess(data))
    } catch (err) {
        console.log(err)
    }
}

export function* addShowtime() {
    yield takeEvery(actions.addShowtime, handleAddShowtime)
}

export function* handleUpdateShowtime(action) {
    try {
        let data = {}
        let error = ""
        console.log(action.payload)
        yield axios({
            method: "put",
            data: action.payload.data,
            url: baseUrl + action.payload._id
        }).then(res => {
            data = res.data
        }).catch(err => {
            error = err
        })
        console.log(data)
        if (error) {
            return yield put(actions.updateShowtimeFail(error))
        }
        console.log(data)
        yield put(actions.updateShowtimeSuccess(data))
    } catch (err) {
        console.log(err)
    }
}

export function* updateShowtime() {
    yield takeEvery(actions.updateShowtime, handleUpdateShowtime)
}

export default {
    getdata,
    getRoom,
    getShowtime,
    addShowtime,
    updateShowtime,
}