import { put, takeEvery } from "redux-saga/effects"
import axios from "axios"
import api from "../../../services/api"
import * as actions from "../actions"

const urlGetRevenueFilm = api.urlGetRevenueFilm
const urlGetRevenueTickets = api.urlGetRevenueTickets
const urlGetRevenueCombo = api.urlGetRevenueCombo

export function* handleGetFilmRevenue(action) {
    try {
        let data = {}
        yield axios({
            method: "get",
            url: urlGetRevenueFilm
        }).then((res) => {
            data = { ...res }
        }).catch(e => {
            console.log(e)
        })
        yield put(actions.getFilmRevenueSuccess(data))
    } catch (e) {
        console.log(e)
    }
}

export function* getFilmRevenue() {
    yield takeEvery(actions.getFilmRevenue, handleGetFilmRevenue)
}

export function* handleGetRevenueTicket(action) {
    try {
        let data = {}
        yield axios({
            method: "get",
            url: urlGetRevenueTickets
        }).then((res) => {
            data = { ...res }
        }).catch(e => {
            console.log(e)
        })
        yield put(actions.getRevenueTicketSuccess(data))
    } catch (e) {
        console.log(e)
    }
}

export function* getRevenueTicket() {
    yield takeEvery(actions.getRevenueTicket, handleGetRevenueTicket)
}

export function* handleGetRevenueCombo(action) {
    try {
        let data = {}
        yield axios({
            method: "get",
            url: urlGetRevenueCombo
        }).then((res) => {
            data = { ...res }
        }).catch(e => {
            console.log(e)
        })
        yield put(actions.getRevenueComboSuccess(data))
    } catch (e) {
        console.log(e)
    }
}

export function* getRevenueCombo() {
    yield takeEvery(actions.getRevenueCombo, handleGetRevenueCombo)
}

export default {
    getFilmRevenue,
    getRevenueTicket,
    getRevenueCombo
}