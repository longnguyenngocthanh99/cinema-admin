import React, { Component } from 'react'
import { Pie } from 'react-chartjs-2';

export default class TicketRevenue extends Component {

    render() {
        const { tickets } = this.props

        const data = {
            labels: tickets.name,
            datasets: [
                {
                    data: tickets.count,
                    backgroundColor: [
                        '#FF6384',
                        '#36A2EB',
                        '#FFCE56',
                        '#FFCE20'
                    ],

                    hoverBackgroundColor: [
                        '#FF6384',
                        '#36A2EB',
                        '#FFCE56',
                        '#FFCE20'
                    ]
                }
            ]
        };
        return (
            <div className="col-12 col-md-4 mt-5 ">
                <div className="card container ">
                    <h2 style={{ textAlign: "center" }}>Lượng khách hàng</h2>
                    <p style={{ textAlign: "center" }}>Tổng lượng khách hàng : <b>{this.props.totalTicket}</b> </p>
                    <Pie data={data} height={175} />
                </div>
            </div>
        )
    }
}
