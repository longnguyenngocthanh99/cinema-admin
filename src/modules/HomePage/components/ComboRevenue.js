import React, { Component } from 'react'
import { Bar } from 'react-chartjs-2';


export default class ComboRevenue extends Component {

    render() {
        const { combo } = this.props
        // var data = !films ? [] : films
        const data = {
            labels: combo.name,
            datasets: [
                {
                    label: 'Doanh thu combo',
                    backgroundColor: '#007bff',
                    borderColor: 'rgba(255,99,132,1)',
                    borderWidth: 1,
                    hoverBackgroundColor: '#0067cc',
                    hoverBorderColor: 'rgba(255,99,132,1)',
                    data: combo.revenue
                }
            ]
        };
        console.log(data)
        return (
            <div className="col-12 col-md-4  mt-5">
                <div className="card container " style={{ height: "400" }}>
                    <h2 style={{ textAlign: "center" }}>Doanh thu combo : </h2>
                    <p style={{ textAlign: "center" }}>Tổng doanh thu combo : <b>{this.props.totalRevenueCombo} Đ</b> </p>
                    <div>
                        <Bar
                            data={data}
                            width={50}
                            height={300}
                            options={{ maintainAspectRatio: false }}
                        />
                    </div>
                </div>

            </div>
        );
    }
}
