import React, { Component } from 'react'
import { Bar } from 'react-chartjs-2';

export default class FilmRevenue extends Component {
    render() {
        const { films } = this.props
        // var data = !films ? [] : films
        const data = {
            labels: films.name,
            datasets: [
                {
                    label: 'Doanh thu của phim',
                    backgroundColor: '#007bff',
                    borderColor: 'rgba(255,99,132,1)',
                    borderWidth: 1,
                    hoverBackgroundColor: '#0067cc',
                    hoverBorderColor: 'rgba(255,99,132,1)',
                    data: films.revenue
                }
            ]
        };
        console.log(data)
        return (
            <div className="col-12 card">
                <h2 style={{ textAlign: "center" }}>Doanh thu các phim</h2>
                <p style={{ textAlign: "center" }}>Tổng doanh thu : <b>{this.props.totalRevenue} Đ</b></p>
                <div>
                    <Bar
                        data={data}
                        width={50}
                        height={300}
                        options={{ maintainAspectRatio: false }}
                    />
                </div>
            </div>
        );
    }
}