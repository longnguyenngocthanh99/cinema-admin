import React, { Component } from 'react'
import { Doughnut } from 'react-chartjs-2';

export default class FilmDoughnut extends Component {
    render() {
        const { films } = this.props

        const data = {
            labels: films.name,
            datasets: [
                {
                    backgroundColor: [
                        '#FF6384',
                        '#36A2EB',
                        '#FFCE56',
                        '#36A2EC',
                        '#FFCE5A',
                        '#36A2EE',
                        '#FFCE5F',
                        '#36A2EG',
                        '#FFCE5H'
                    ],

                    hoverBackgroundColor: [
                        '#FF6384',
                        '#36A2EB',
                        '#FFCE56',
                        '#36A2EC',
                        '#FFCE5A',
                        '#36A2EE',
                        '#FFCE5F',
                        '#36A2EG',
                        '#FFCE5H'
                    ],
                    data: films.revenue ? films.revenue.map(e => e / this.props.totalRevenue * 100) : []
                }
            ]
        };
        return (
            <div className="col-12 col-md-4  mt-5">
                <div className="card container ">
                    <h2 style={{ textAlign: "center" }}>Phần trăm của doanh thu phim</h2>
                    <Doughnut data={data} height={200} />

                </div>
            </div>
        )
    }
}
