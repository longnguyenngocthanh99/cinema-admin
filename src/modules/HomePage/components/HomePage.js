import React, { Component } from 'react'
import FilmRevenue from "./FilmRevenue"
import FilmDoughnut from "./FilmDoughnut"
import TicketRevenue from "./TicketRevenue"
import ComboRevenue from "./ComboRevenue"

export default class LoginPage extends Component {
    componentDidMount() {
        this.props.actions.getFilmRevenue()
        this.props.actions.getRevenueTicket()
        this.props.actions.getRevenueCombo()
    }
    constructor(props) {
        super(props)
        this.state = {
            openMenu: false
        }
    }
    render() {
        console.log(this.props)
        const handleLogin = () => {
            this.props.actions.login({
                username: this.props.data.username,
                password: this.props.data.password
            })
        }
        return (
            <div className="row">
                <FilmRevenue {...this.props} />
                <FilmDoughnut {...this.props} />
                <TicketRevenue {...this.props} />
                <ComboRevenue {...this.props} />
            </div>
        )
    }
}
