import { createAction } from "redux-actions"
import * as CONST from "./constants"

export const getFilmRevenue = createAction(CONST.GET_FILM_REVEUE)
export const getFilmRevenueSuccess = createAction(CONST.GET_FILM_REVEUE_SUCCESS)
export const getFilmRevenueFail = createAction(CONST.GET_FILM_REVEUE_FAIL)

export const getRevenueTicket = createAction(CONST.GET_FILM_REVEUE_TICKETS)
export const getRevenueTicketSuccess = createAction(CONST.GET_FILM_REVEUE_TICKETS_SUCCESS)
export const getRevenueTicketFail = createAction(CONST.GET_FILM_REVEUE_TICKETS_FAIL)

export const getRevenueCombo = createAction(CONST.GET_FILM_REVEUE_COMBO)
export const getRevenueComboSuccess = createAction(CONST.GET_FILM_REVEUE_COMBO_SUCCESS)
export const getRevenueComboFail = createAction(CONST.GET_FILM_REVEUE_COMBO_FAIL)