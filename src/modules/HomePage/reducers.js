import freeze from "deep-freeze"
import { handleActions } from "redux-actions"
import * as actions from "./actions"

export const name = "HomePage"

const intialState = freeze({
    films: [],
    totalRevenue: 1,
    tickets: [],
    totalTicket: 0,
    combo: [],
    totalRevenueCombo: 0,

})

export default handleActions({
    [actions.getFilmRevenueSuccess]: (state, action) => {
        return freeze({
            ...state,
            films: action.payload.data,
            totalRevenue: action.payload.data.revenue.reduce((a, b) => a + b)
        })
    },
    [actions.getRevenueTicketSuccess]: (state, action) => {
        return freeze({
            ...state,
            tickets: action.payload.data,
            totalTicket: action.payload.data.count.reduce((a, b) => a + b)
        })
    },
    [actions.getRevenueComboSuccess]: (state, action) => {
        return freeze({
            ...state,
            combo: action.payload.data,
            totalRevenueCombo: action.payload.data.revenue.reduce((a, b) => a + b)
        })
    }
},
    intialState
)