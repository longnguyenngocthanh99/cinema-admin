import { put, takeEvery } from "redux-saga/effects"
import axios from "axios"
import api from "../../../services/api"
import * as actions from "../actions"

const baseUrl = api.urlCinema
const urlTheater = api.urlTheater

export function* handleGetCinema() {
    try {
        let data = {}
        yield axios({
            method: "get",
            url: baseUrl,
        }).then((res) => {
            console.log(res)
            data.movies = res.data
        }).catch(e => {
            console.log(e)
        })
        yield put(actions.getCinemaSuccess(data))
    } catch (e) {
        console.log(e)
    }
}

export function* getCinema() {
    yield takeEvery(actions.getCinema, handleGetCinema)
}

export function* handleGetTheater(action) {
    try {
        console.log(action)
        let data = {}
        yield axios({
            method: "get",
            url: baseUrl + action.payload,
        }).then((res) => {
            console.log(res)
            data = res.data
        }).catch(e => {
            console.log(e)
        })
        yield put(actions.getTheaterSuccess(data))
    } catch (e) {
        console.log(e)
    }
}

export function* getTheater() {
    yield takeEvery(actions.getTheater, handleGetTheater)
}

export function* handleAddTheater(action) {
    try {
        console.log(action)
        let data = {}
        yield axios({
            method: "post",
            url: urlTheater,
            data: action.payload.data
        }).then((res) => {
            data = res.data
            console.log(data)
        }).catch(e => {
            console.log(e)
        })
        yield put(actions.handleAddNewTheaterSuccess(data))
    } catch (e) {
        console.log(e)
    }
}

export function* addNewTheater() {
    yield takeEvery(actions.handleAddNewTheater, handleAddTheater)
}

export function* handleUpdateTheater(action) {
    try {
        console.log(action)
        let data = {}
        yield axios({
            method: "put",
            url: urlTheater,
            data: action.payload.data
        }).then((res) => {
            console.log(res)
            data = res.data
        }).catch(e => {
            console.log(e)
        })
        yield put(actions.updateTheaterSuccess(data))
    } catch (e) {
        console.log(e)
    }
}

export function* updateTheater() {
    yield takeEvery(actions.updateTheater, handleUpdateTheater)
}

export function* handleAddCinema(action) {
    try {
        console.log(action)
        let data = {}
        yield axios({
            method: "post",
            url: baseUrl,
            data: action.payload.data
        }).then((res) => {
            data = res.data
            console.log(data)
        }).catch(e => {
            console.log(e)
        })
        yield put(actions.addCinemaSuccess(data))
    } catch (e) {
        console.log(e)
    }
}

export function* addCinema() {
    yield takeEvery(actions.addCinema, handleAddCinema)
}

export function* handleUpdateCinema(action) {
    try {
        console.log(action)
        let data = {}
        yield axios({
            method: "put",
            url: baseUrl,
            data: action.payload.data
        }).then((res) => {
            console.log(res)
            data = res.data
        }).catch(e => {
            console.log(e)
        })
        yield put(actions.updateCinemaSuccess(data))
    } catch (e) {
        console.log(e)
    }
}

export function* updateCinema() {
    yield takeEvery(actions.updateCinema, handleUpdateCinema)
}


export default {
    getCinema,
    addCinema,
    updateCinema,
    getTheater,
    addNewTheater,
    updateTheater,
}