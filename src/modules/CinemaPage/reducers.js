import freeze from "deep-freeze"
import { handleActions } from "redux-actions"
import * as actions from "./actions"

export const name = "CinemaPage"

const intialState = freeze({
    cinema: [],
    theaters: [],
    data: {
        address: "",
        name: "",
    },
    isTableCinema: true,
    openLoadingDialog: false,
    openMessageDialog: false,
    formStatus: true,
    addingNewTheater: false
})

export default handleActions({
    [actions.handleFormStatus]: (state, action) => {
        return freeze({
            ...state,
            formStatus: action.payload
        })
    },
    [actions.handleChangeText]: (state, action) => {
        return freeze({
            ...state,
            data: {
                ...state.data,
                [action.payload.name]: action.payload.value
            }
        })
    },
    [actions.handleChangeTheater]: (state, action) => {
        let oldTheater = state.theaters
        let newTheaters = []
        oldTheater.map(e => {
            if (e._id === action.payload._id) {
                let temp = { ...e }
                temp[action.payload.data.name] = action.payload.data.value
                return newTheaters.push(temp)
            }
            return newTheaters.push(e)
        })
        console.log(newTheaters)
        return freeze({
            ...state,
            theaters: [
                ...newTheaters,
            ]
        })
    },
    [actions.handleDialogMessage]: (state, action) => {
        return freeze({
            ...state,
            openMessageDialog: action.payload
        })
    },
    [actions.getCinemaSuccess]: (state, action) => {
        return freeze({
            ...state,
            cinema: action.payload.movies
        })
    },
    [actions.getTheaterSuccess]: (state, action) => {
        return freeze({
            ...state,
            theaters: action.payload,
            addingNewTheater: false
        })
    },
    [actions.addCinemaSuccess]: (state, action) => {
        return freeze({
            ...state,
            openLoadingDialog: false,
            openMessageDialog: true,
            formStatus: true,
            data: {
                ...action.payload
            }
        })
    },
    [actions.updateCinemaSuccess]: (state, action) => {
        return freeze({
            ...state,
            openLoadingDialog: false,
            openMessageDialog: true,
            formStatus: true
        })
    },
    [actions.updateTheaterSuccess]: (state, action) => {
        return freeze({
            ...state,
            openLoadingDialog: false,
            openMessageDialog: true,
            formStatus: true
        })
    },
    [actions.handleAddNewTheaterSuccess]: (state, action) => {
        return freeze({
            ...state,
            openLoadingDialog: false,
            openMessageDialog: true,
            formStatus: true,
            addingNewTheater: false,
        })
    },
    [actions.handleIsTableCinema]: (state, action) => {
        return freeze({
            ...state,
            isTableCinema: action.payload
        })
    },
    [actions.handleInitData]: (state, action) => {
        return freeze({
            ...state,
            data: intialState.data,
            imgShow1: "",
            imgShow2: ""
        })
    },
    [actions.handleGetInfoCinema]: (state, action) => {
        return freeze({
            ...state,
            data: {
                ...action.payload,
            },
        })
    },
    [actions.handleInitNewTheater]: (state, action) => {
        let oldTheater = state.theaters
        let newTheaters = []
        oldTheater.map(e => {
            newTheaters.push(e)
        })
        newTheaters.push({
            name: "",
            seatNumber: "",
            status: "ready",
            idCinema: state.data._id
        })
        console.log(newTheaters)
        return freeze({
            ...state,
            theaters: newTheaters,
            addingNewTheater: true
        })
    },
},
    intialState
)