import { createAction } from "redux-actions"
import * as CONST from "./constants"

export const getCinema = createAction(CONST.GET_CINEMA)
export const getCinemaSuccess = createAction(CONST.GET_CINEMA_SUCCESS)
export const getCinemaFail = createAction(CONST.GET_CINEMA_FAIL)

export const getTheater = createAction(CONST.GET_THEATER)
export const getTheaterSuccess = createAction(CONST.GET_THEATER_SUCCESS)
export const getTheaterFail = createAction(CONST.GET_THEATER_FAIL)

export const updateTheater = createAction(CONST.UPDATE_THEATER)
export const updateTheaterSuccess = createAction(CONST.UPDATE_THEATER_SUCCESS)
export const updateTheaterFail = createAction(CONST.UPDATE_THEATER_FAIL)

export const updateCinema = createAction(CONST.UPDATE_CINEMA)
export const updateCinemaSuccess = createAction(CONST.UPDATE_CINEMA_SUCCESS)
export const updateCinemaFail = createAction(CONST.UPDATE_CINEMA_FAIL)

export const addCinema = createAction(CONST.ADD_CINEMA)
export const addCinemaSuccess = createAction(CONST.ADD_CINEMA_SUCCESS)
export const addCinemaFail = createAction(CONST.ADD_CINEMA_FAIL)

export const getFile = createAction(CONST.GET_FILE);
export const handleIsUpdate = createAction(CONST.HANDLE_IS_UPDATE)
export const handleLoadUpdate = createAction(CONST.HANDLE_LOAD_UPDATE)

export const handleAddNewTheater = createAction(CONST.HANDLE_ADD_NEW_THEATER)
export const handleAddNewTheaterSuccess = createAction(CONST.HANDLE_ADD_NEW_THEATER_SUCCESS)
export const handleAddNewTheaterFail = createAction(CONST.HANDLE_ADD_NEW_THEATER_FAIL)

export const handleInitData = createAction(CONST.HANDLE_INIT_DATA)
export const handleDialogMessage = createAction(CONST.HANDLE_DIALOG_MESSAGE)
export const handleChangeText = createAction(CONST.HANDLECHANGETEXT)
export const handleChangeTheater = createAction(CONST.HANDLE_CHANGE_THEATER)
export const handleChangeDate = createAction(CONST.HANDLECHANGEDATE)
export const handleIsTableCinema = createAction(CONST.HANDLE_IS_TABLE_CINEMA)
export const handleGetInfoCinema = createAction(CONST.HANDLE_GET_INFO_CINEMA)
export const handleFormStatus = createAction(CONST.HANDLE_FORM_STATUS)
export const handleInitNewTheater = createAction(CONST.HANDLE_INIT_NEW_THEATER)