import React, { Component } from 'react'
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import LoadingDialog from "../../common/loadingDialog"
import MessageDialog from "../../common/messageDialog"

export default class FormFilm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            openLoadingDialog: false
        }
    }
    componentDidMount() {
        if (this.props.data._id)
            this.props.actions.getTheater(this.props.data._id)
    }
    render() {
        console.log(this.props)
        const submitTheater = (index) => {
            const theater = this.props.theaters[index]
            for (let i in theater) {
                if (theater[i] === "")
                    return this.props.actions.handleFormStatus(false)
            }
            if (theater._id) {
                this.props.actions.updateTheater({ data: theater })
            }
            else {
                this.props.actions.handleAddNewTheater({ data: theater })
            }
        }

        const submitCinema = () => {
            const cinema = this.props.data
            for (let i in cinema) {
                if (cinema[i] === "")
                    return this.props.actions.handleFormStatus(false)
            }
            if (cinema._id) {
                this.props.actions.updateCinema({ data: cinema })
            }
            else {
                this.props.actions.addCinema({ data: cinema })
            }
        }

        const displayRoom = () => {
            const { theaters } = this.props
            return theaters.map((theater, i) => {
                console.log(theater._id)
                return <div key={i} >
                    <div data-parsley-validate className="row" >
                        <div className="form-group col-md-3">
                            <input type="text" name="name" parsley-trigger="change"
                                placeholder="Nhập tên phim" className="form-control" id="userName"
                                value={theater.name}
                                onChange={e => this.props.actions.handleChangeTheater({
                                    _id: theater._id,
                                    data: { name: e.target.name, value: e.target.value }
                                })} />
                            {!this.props.formStatus && theater.name === "" ? <p style={{ color: "red" }}>Hãy nhập tên rạp</p> : ""}
                        </div>
                        <div className="form-group col-md-3">
                            <input type="number" name="seatNumber" parsley-trigger="change"
                                placeholder="Nhập địa chỉ" className="form-control" id="seatNumber"
                                value={theater.seatNumber}
                                onChange={e => this.props.actions.handleChangeTheater({
                                    _id: theater._id,
                                    data: { name: e.target.name, value: e.target.value }
                                })} />
                            {!this.props.formStatus && theater.seatNumber === "" ? <p style={{ color: "red" }}>Hãy nhập số lượng ghế</p> : ""}
                        </div>
                        <div className="form-group col-md-3">
                            <select className="custom-select" name="status"
                                value={theater.status}
                                onChange={e => this.props.actions.handleChangeTheater({
                                    _id: theater._id,
                                    data: { name: e.target.name, value: e.target.value }
                                })}>
                                <option value="ready">Sẵn sàn</option>
                                <option value="waiting">Bảo trì</option>
                            </select>
                        </div>
                        <div className="form-group col-md-3">
                            <button className="btn btn-primary waves-effect waves-light" onClick={e => {
                                submitTheater(i)
                            }}>
                                Submit
                            </button>
                        </div>
                    </div>

                </div >
            })
        }
        console.log(this.props)
        const {
            name,
            address } = this.props.data;
        const { imgShow1, imgShow2 } = this.props
        return (
            <div className="row">
                <div className="col-xl-12">
                    <div className="card-box">
                        <h3 className="mb-3">
                            <i className="fa fa-arrow-left" style={{ fontSize: "1.2rem", marginRight: "1rem", cursor: "pointer" }} onClick={e => this.props.actions.handleIsTableCinema(true)}></i>
                            <b>Thông tin Cinema</b>
                        </h3>

                        <form data-parsley-validate className="row">
                            <div className="form-group col-md-6">
                                <label htmlFor="userName">Tên cinema*</label>
                                <input type="text" name="name" parsley-trigger="change" required
                                    placeholder="Nhập tên cinema" className="form-control" id="userName"
                                    value={name}
                                    onChange={e => this.props.actions.handleChangeText(e.target)} />
                                {!this.props.formStatus && name === "" ? <p style={{ color: "red" }}>Hãy nhập tên cinema</p> : ""}
                            </div>
                            <div className="form-group col-md-6">
                                <label htmlFor="userName">Địa chỉ*</label>
                                <input type="text" name="address" parsley-trigger="change" required
                                    placeholder="Nhập địa chỉ" className="form-control" id="userName"
                                    value={address}
                                    onChange={e => this.props.actions.handleChangeText(e.target)} />
                                {!this.props.formStatus && address === "" ? <p style={{ color: "red" }}>Hãy nhập địa chỉ</p> : ""}
                            </div>

                        </form>
                        <div className="form-group text-right m-b-0">
                            <button className="btn btn-primary waves-effect waves-light" onClick={e => {
                                submitCinema()
                            }}>
                                Submit
                            </button>
                            <button className="btn btn-secondary waves-effect waves-light m-l-5" onClick={e => {
                                this.props.actions.handleIsTableCinema(true)
                            }}>
                                Cancel
                            </button>
                        </div>
                        <LoadingDialog open={this.state.openLoadingDialog} />
                        <MessageDialog message="Save success" handleDialog={this.props.actions.handleDialogMessage} open={this.props.openMessageDialog} />
                    </div>
                    {this.props.data._id ?
                        <div className="card-box">
                            <h3 className="mb-3">
                                <b>Danh sách rạp</b>
                            </h3>
                            <form data-parsley-validate className="row">
                                <div className="form-group col-md-3">
                                    <label>Tên rạp</label>
                                </div>
                                <div className="form-group col-md-3">
                                    <label>Số lượng ghế</label>

                                </div>
                                <div className="form-group col-md-3">
                                    <label>Trạng thái</label>
                                </div>
                            </form>
                            {displayRoom()}{
                                !this.props.addingNewTheater ?
                                    <button className="btn btn-secondary waves-effect waves-light m-l-5" onClick={e => {
                                        this.props.actions.handleInitNewTheater()
                                    }}>
                                        Thêm rạp <i className="fa fa-plus ml-2" />
                                    </button> : ""
                            }
                        </div> : ""}
                </div>

            </div >
        )
    }
}
