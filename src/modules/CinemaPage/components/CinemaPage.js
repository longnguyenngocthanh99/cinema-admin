import React, { Component } from 'react'
import CinemaTable from "./CinemaTable"
import FormCinema from "./CinemaForm"

export default class Cinema extends Component {
    render() {
        return this.props.isTableCinema ? <CinemaTable {...this.props} /> : <FormCinema {...this.props} />
    }
}
