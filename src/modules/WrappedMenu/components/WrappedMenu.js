import React, { Component } from 'react'

export default class LoginPage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            openMenu: false
        }
    }
    render() {
        console.log(this.props)
        const handleLogin = () => {
            this.props.actions.login({
                username: this.props.data.username,
                password: this.props.data.password
            })
        }
        return (
            <div id="wrapper" className={this.state.openMenu ? "forced" : "enlarged"} >

                <div className="topbar">
                    <div className="topbar-left">
                        <a href="index.html" className="logo"><span>Admin<span>to</span></span><i className="mdi mdi-layers"></i></a>
                    </div>

                    <div className="navbar navbar-default" role="navigation">
                        <div className="container-fluid">
                            <ul className="nav navbar-nav list-inline navbar-left">
                                <li className="list-inline-item" onClick={e => this.setState({ openMenu: !this.state.openMenu })}>
                                    <button className="button-menu-mobile open-left">
                                        <i className="mdi mdi-menu"></i>
                                    </button>
                                </li>
                                <li className="list-inline-item">
                                    <h4 className="page-title">Dashboard</h4>
                                </li>
                            </ul>

                            <nav className="navbar-custom">

                                <ul className="list-unstyled topbar-right-menu float-right mb-0">

                                    <li>
                                        <div className="notification-box">
                                            <ul className="list-inline mb-0">
                                                <li>
                                                    <div className="noti-dot">
                                                        <span className="dot"></span>
                                                        <span className="pulse"></span>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>

                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
                <div className="left side-menu">
                    <div className="sidebar-inner slimscrollleft">

                        <div className="user-box">
                            <div className="user-img">
                                <img src="assets/images/users/avatar-1.jpg" alt="user-img" title="Mat Helme" className="rounded-circle img-thumbnail img-responsive" />
                                <div className="user-status offline"><i className="mdi mdi-adjust"></i></div>
                            </div>
                            <h5><a >Mat Helme</a> </h5>
                            <ul className="list-inline">
                                <li className="list-inline-item">
                                    <a  >
                                        <i className="mdi mdi-settings"></i>
                                    </a>
                                </li>

                                <li className="list-inline-item">
                                    <a className="text-custom">
                                        <i className="mdi mdi-power" onClick={e => this.setState({ openMenu: !this.state.openMenu })}></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div id="sidebar-menu">
                            <ul>

                                <li>
                                    <a className={this.props.location.pathname === "/" ? "waves-effect active" : "waves-effect"} onClick={e => this.props.history.push("/")}>
                                        <i className="mdi mdi-view-dashboard" />
                                        <span> Dashboard </span>
                                    </a>
                                </li>

                                <li >
                                    <a className={this.props.location.pathname === "/film" ? "waves-effect active" : "waves-effect"} onClick={e => {
                                        this.props.actions.handleIsTableFilm(true)
                                        this.props.history.push("/film")
                                    }}>
                                        <i className="fa fa-film" />
                                        <span> Phim </span>
                                    </a>
                                </li>

                                <li>
                                    <a className={this.props.location.pathname === "/showtime" ? "waves-effect active" : "waves-effect"} onClick={e => this.props.history.push("/showtime")}>
                                        <i className="fa fa-ticket" />
                                        <span> Xuất chiếu </span>
                                    </a>
                                </li>

                                <li>
                                    <a className={this.props.location.pathname === "/cinema" ? "waves-effect active" : "waves-effect"} onClick={e => this.props.history.push("/cinema")}>
                                        <i className="fa fa-building" />
                                        <span> Rạp Phim </span>
                                    </a>
                                </li>

                                <li>
                                    <a className={this.props.location.pathname === "/member" ? "waves-effect active" : "waves-effect"} onClick={e => this.props.history.push("/member")}>
                                        <i className="fa fa-user" />
                                        <span> Thành Viên </span>
                                    </a>
                                </li>
                            </ul>
                            <div className="clearfix"></div>
                        </div>
                        <div className="clearfix"></div>

                    </div>

                </div>

                <div className="content-page">
                    <div className="content">
                        <div className="container-fluid">{this.props.children}</div>
                    </div>

                    <footer className="footer text-right">
                        2016 - 2019 © Adminto. Coderthemes.com
                    </footer>

                </div>


            </div>
        )
    }
}
