import freeze from "deep-freeze"
import { handleActions } from "redux-actions"
import * as actions from "./actions"

export const name = "WrappedMenu"

const intialState = freeze({

})

export default handleActions({
    [actions.loginSuccess]: (state, action) => {
        return freeze({
            ...state,
            userInfo: action.payload.data
        })
    },
    [actions.handleChangeText]: (state, action) => {
        return freeze({
            ...state,
            data: {
                ...state.data,
                [action.payload.name]: action.payload.value
            }
        })
    }
},
    intialState
)