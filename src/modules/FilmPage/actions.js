import { createAction } from "redux-actions"
import * as CONST from "./constants"

export const getMovies = createAction(CONST.GET_MOVIES)
export const getMoviesSuccess = createAction(CONST.GET_MOVIES_SUCCESS)
export const getMoviesFail = createAction(CONST.GET_MOVIES_FAIL)

export const getRevenue = createAction(CONST.GET_REVENUE)
export const getRevenueSuccess = createAction(CONST.GET_REVENUE_SUCCESS)
export const getRevenueFail = createAction(CONST.GET_REVENUE_FAIL)

export const updateFilm = createAction(CONST.UPDATE_FILM)
export const updateFilmSuccess = createAction(CONST.UPDATE_FILM_SUCCESS)
export const updateFilmFail = createAction(CONST.UPDATE_FILM_FAIL)

export const addFilm = createAction(CONST.ADD_FILM)
export const addFilmSuccess = createAction(CONST.ADD_FILM_SUCCESS)
export const addFilmFail = createAction(CONST.ADD_FILM_FAIL)

export const getFile = createAction(CONST.GET_FILE);
export const handleIsUpdate = createAction(CONST.HANDLE_IS_UPDATE)
export const handleLoadUpdate = createAction(CONST.HANDLE_LOAD_UPDATE)

export const handleInitData = createAction(CONST.HANDLE_INIT_DATA)
export const handleDialogMessage = createAction(CONST.HANDLE_DIALOG_MESSAGE)
export const handleChangeText = createAction(CONST.HANDLECHANGETEXT)
export const handleChangeDate = createAction(CONST.HANDLECHANGEDATE)
export const handleIsTableFilm = createAction(CONST.HANDLE_IS_TABLE_FILM)
export const handleGetInfoFilm = createAction(CONST.HANDLE_GET_INFO_FILM)
export const handleFormStatus = createAction(CONST.HANDLE_FORM_STATUS)