import { put, takeEvery } from "redux-saga/effects"
import axios from "axios"
import api from "../../../services/api"
import * as actions from "../actions"

const baseUrl = api.urlFilm
const urlUpdateFilm = api.urlFilmUpdate
const urlGetVenue = api.urlGetRevenue

export function* handleGetMovies() {
    try {
        let data = {}
        yield axios({
            method: "get",
            url: baseUrl,
        }).then((res) => {
            console.log(res)
            data.movies = res.data
        }).catch(e => {
            console.log(e)
        })
        yield put(actions.getMoviesSuccess(data))
    } catch (e) {
        console.log(e)
    }
}

export function* getMovies() {
    yield takeEvery(actions.getMovies, handleGetMovies)
}

export function* handleGetRevenue(action) {
    try {
        console.log(action)
        let data = {}
        yield axios({
            method: "get",
            url: urlGetVenue + action.payload,
        }).then((res) => {
            console.log(res)
            let temp = 0
            data.revenue = res.data.reduce((a, b) => a + b['revenueTickets'], 0)
        }).catch(e => {
            console.log(e)
        })
        yield put(actions.getRevenueSuccess(data))
    } catch (e) {
        console.log(e)
    }
}

export function* getRevenue() {
    yield takeEvery(actions.getRevenue, handleGetRevenue)
}

export function* handleUpdateFilm(action) {
    try {
        console.log(action)
        let formData = new FormData()
        const data = action.payload.data
        console.log(data)
        for (let i in data) {
            formData.append(i.toString(), data[i])
        }
        yield axios({
            method: "put",
            url: urlUpdateFilm + data._id,
            data: formData
        }).then((res) => {
            data = res.data
            console.log(data)
        }).catch(e => {
            console.log(e)
        })
        yield put(actions.updateFilmSuccess(data))
    } catch (e) {
        console.log(e)
    }
}

export function* updateFilm() {
    yield takeEvery(actions.updateFilm, handleUpdateFilm)
}

export function* handleAddFilm(action) {
    try {
        console.log(action)
        let formData = new FormData()
        const data = action.payload.data
        console.log(data)
        for (let i in data) {
            formData.append(i.toString(), data[i])
        }
        console.log(action.payload)
        yield axios({
            method: "post",
            url: urlUpdateFilm,
            data: formData
        }).then((res) => {
            data = res.data
            console.log(data)
        }).catch(e => {
            console.log(e)
        })
        yield put(actions.addFilmSuccess(data))
    } catch (e) {
        console.log(e)
    }
}

export function* addFilm() {
    yield takeEvery(actions.addFilm, handleAddFilm)
}

export default {
    getMovies,
    getRevenue,
    updateFilm,
    addFilm
}