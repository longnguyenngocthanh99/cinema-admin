import React from "react"
import { bindActionCreators } from "redux"
import { connect } from "react-redux"
import { withRouter } from "react-router-dom"
import { name } from "../reducers"
import * as action from "../actions"
import FilmPage from "./FilmPage"

class FilmPageContainer extends React.Component {
    componentDidMount() {
        this.props.actions.handleIsTableFilm(true)
    }
    render() {
        return (
            <React.Fragment>
                <FilmPage {...this.props} />
            </React.Fragment>
        )
    }
}

function mapStateToProps(state) {
    return {
        ...state[name]
    }
}

function mapDispatchToProps(dispatch) {
    const actions = {
        ...action,
    }
    return { actions: bindActionCreators(actions, dispatch) }
}

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(FilmPageContainer)
)