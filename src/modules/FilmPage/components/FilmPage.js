import React, { Component } from 'react'
import FilmTable from "./FilmTable"
import FormFilm from "./FormFilm"

export default class LoginPage extends Component {
    render() {
        return this.props.isTableFilm ? <FilmTable {...this.props} /> : <FormFilm {...this.props} />
    }
}
