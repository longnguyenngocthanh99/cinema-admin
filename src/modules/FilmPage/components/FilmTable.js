import React, { Component } from 'react'
import Paper from "@material-ui/core/Paper";
import // State or Local Processing Plugins
    "@devexpress/dx-react-grid";
import {
    Grid,
    Table,
    TableHeaderRow,
    PagingPanel,
    SearchPanel,
    Toolbar,
    TableSelection
} from "@devexpress/dx-react-grid-material-ui";
import {
    SortingState,
    PagingState,
    IntegratedSorting,
    IntegratedPaging,
    IntegratedFiltering,
    SearchState,
    SelectionState,
} from '@devexpress/dx-react-grid';

export default class FilmTable extends Component {
    constructor(props) {
        super(props);
        console.log(this.props.movies)

        this.state = {
            pageSizes: 5,
            columns: [
                { name: "name", title: "Tên" },
                { name: "status", title: "Trạng thái" },
                { name: "dayShow", title: "Ngày chiếu" },
                { name: "time", title: "Thời gian" },
                { name: "rate", title: "Điểm" },
            ],
            rows: this.props.movies ? this.props.movies : [],
            selection: []
        };
    }
    componentDidMount() {
        this.props.actions.getMovies()
    }
    render() {
        console.log(this.props)
        const handleLogin = () => {
            this.props.actions.login({
                username: this.props.data.username,
                password: this.props.data.password
            })
        }
        const customComponent = (e) => {
            return <td className="btn waves-effect mt-1" onClick={() => {
                this.props.actions.handleGetInfoFilm(e.row)
                this.props.actions.handleIsTableFilm(false)
            }}><i className="fa fa-search" ></i></td>
        }
        return (
            <div className="row">
                <div className="col-12">
                    <button type="button" className="btn btn-secondary waves-effect w-md m-b-5" onClick={e => {
                        this.props.actions.handleInitData()
                        this.props.actions.handleIsTableFilm(false)
                    }}>Thêm phim mới <i className="fa fa-plus ml-2" /></button>
                    <Paper>
                        <Grid rows={[...this.props.movies]} columns={this.state.columns}>
                            <SearchState />
                            <SelectionState
                                selection={this.state.selection}
                            />
                            <PagingState
                                defaultCurrentPage={0}
                                pageSize={this.state.pageSizes}
                                onPageSizeChange={e => { this.setState({ pageSizes: e }) }}
                            />
                            <SortingState
                                defaultSorting={[{ columnName: 'status', direction: 'asc' }]}
                            />
                            <IntegratedFiltering />
                            <IntegratedSorting />
                            <IntegratedPaging />
                            <Table />
                            <TableHeaderRow showSortingControls />
                            <Toolbar />
                            <SearchPanel />
                            <TableSelection
                                cellComponent={customComponent}
                            />
                            <PagingPanel pageSizes={[5, 10]} />
                        </Grid>
                    </Paper>
                </div>
            </div>
        )
    }
}
