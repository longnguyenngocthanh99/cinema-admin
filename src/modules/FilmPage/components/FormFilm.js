import React, { Component } from 'react'
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import LoadingDialog from "../../common/loadingDialog"
import MessageDialog from "../../common/messageDialog"

export default class FormFilm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            openLoadingDialog: false
        }
    }
    componentDidMount() {
        if (this.props.data._id)
            this.props.actions.getRevenue(this.props.data._id)
    }
    render() {
        const updateFilm = () => {
            const data = this.props.data
            for (let i in data) {
                if (data[i] === "")
                    return this.props.actions.handleFormStatus(false)
            }
            this.props.actions.updateFilm({ data: this.props.data, _id: this.props.data._id })
        }
        const addFilm = () => {
            const data = this.props.data
            for (let i in data) {
                if (data[i] === "")
                    return this.props.actions.handleFormStatus(false)
            }
            this.props.actions.addFilm({ data: this.props.data })
        }
        console.log(this.props)
        const {
            time,
            content,
            name,
            country,
            director,
            status,
            trailer,
            dayShow } = this.props.data;
        const { imgShow1, imgShow2 } = this.props
        return (
            <div className="row">
                <div className="col-xl-12">
                    <div className="card-box">
                        <h3 className="mb-3">
                            <i className="fa fa-arrow-left" style={{ fontSize: "1.2rem", marginRight: "1rem", cursor: "pointer" }} onClick={e => this.props.actions.handleIsTableFilm(true)}></i>
                            <b>Thông tin phim</b>
                        </h3>

                        <form data-parsley-validate className="row">
                            <div className="form-group col-md-6">
                                <label htmlFor="inputAddress2" className="col-form-label">
                                    Hình ảnh dài
                                </label>
                                <br />
                                <input
                                    type="file"
                                    className="dropify"
                                    data-max-file-size="1M"
                                    name="img1"
                                    onChange={e => this.props.actions.getFile(e.target)}
                                />
                                {imgShow1 ? <img src={imgShow1} style={{ width: "100px" }} /> : ""}
                                {!this.props.formStatus && imgShow1 === "" ? <p style={{ color: "red" }}>Hãy đăng hình ảnh</p> : ""}
                            </div>
                            <div className="form-group col-md-6">
                                <label htmlFor="inputAddress2" className="col-form-label">
                                    Hình ảnh rộng
                                </label>
                                <br />
                                <input
                                    type="file"
                                    className="dropify"
                                    data-max-file-size="1M"
                                    name="img2"
                                    onChange={e => this.props.actions.getFile(e.target)}
                                />
                                {imgShow2 ? <img src={imgShow2} style={{ width: "100px" }} /> : ""}
                                {!this.props.formStatus && imgShow2 === "" ? <p style={{ color: "red" }}>Hãy đăng hình ảnh</p> : ""}
                            </div>
                            <div className="form-group col-md-6">
                                <label htmlFor="userName">Tên phim*</label>
                                <input type="text" name="name" parsley-trigger="change" required
                                    placeholder="Nhập tên phim" className="form-control" id="userName"
                                    value={name}
                                    onChange={e => this.props.actions.handleChangeText(e.target)} />
                                {!this.props.formStatus && name === "" ? <p style={{ color: "red" }}>Hãy nhập tên phim</p> : ""}
                            </div>
                            <div className="form-group col-md-6">
                                <label htmlFor="userName">Tác giả*</label>
                                <input type="text" name="director" parsley-trigger="change" required
                                    placeholder="Nhập tên đạo diễn" className="form-control" id="userName"
                                    value={director}
                                    onChange={e => this.props.actions.handleChangeText(e.target)} />
                                {!this.props.formStatus && director === "" ? <p style={{ color: "red" }}>Hãy nhập tên đạo diễn</p> : ""}
                            </div>

                            <div className="form-group col-6">
                                <label htmlFor="userName">Ngày chiếu*</label>
                                <br />
                                <DatePicker
                                    selected={this.props.dateForm}
                                    onChange={e => this.props.actions.handleChangeDate(e)}
                                    dateFormat="dd/MM/yyyy"
                                    peekNextMonth
                                    showMonthDropdown
                                    showYearDropdown
                                    dropdownMode="select"
                                />
                                {!this.props.formStatus && dayShow === "" ? <p style={{ color: "red" }}>Hãy chọn ngày chiếu</p> : ""}
                            </div>

                            <div className="form-group col-6">
                                <label htmlFor="userName">Tình trạng*</label>
                                <select className="custom-select" name="status"
                                    value={status}
                                    onChange={e => this.props.actions.handleChangeText(e.target)}>
                                    <option value="coming">Sắp chiếu</option>
                                    <option value="available">Đang chiếu</option>
                                </select>
                            </div>

                            <div className="form-group col-6">
                                <label htmlFor="userName">Thời gian*</label>
                                <input type="number" name="time" parsley-trigger="change" required
                                    placeholder="Nhập thời gian" className="form-control" id="userName"
                                    value={time}
                                    onChange={e => this.props.actions.handleChangeText(e.target)} />
                                {!this.props.formStatus && time === "" ? <p style={{ color: "red" }}>Hãy nhập thời gian</p> : ""}
                            </div>

                            <div className="form-group col-6">
                                <label htmlFor="userName">Nước *</label>
                                <input type="text" name="country" parsley-trigger="change" required
                                    placeholder="Nhập tên nước" className="form-control" id="userName"
                                    value={country}
                                    onChange={e => this.props.actions.handleChangeText(e.target)} />
                                {!this.props.formStatus && country === "" ? <p style={{ color: "red" }}>Hãy nhập tên quốc gia</p> : ""}
                            </div>

                            <div className="form-group col-md-12">
                                <label htmlFor="userName">Link trailer*</label>
                                <input type="text" name="trailer" parsley-trigger="change" required
                                    placeholder="Nhập link trailer" className="form-control" id="userName"
                                    value={trailer}
                                    onChange={e => this.props.actions.handleChangeText(e.target)} />
                                {!this.props.formStatus && trailer === "" ? <p style={{ color: "red" }}>Hãy nhập link trailer</p> : ""}
                            </div>
                            <div className="form-group col-md-12">
                                <label className=" col-form-label">Nội dung phim*</label>
                                <textarea className="form-control" rows="5"
                                    name="content"
                                    value={content}
                                    onChange={e => this.props.actions.handleChangeText(e.target)}></textarea>
                                {!this.props.formStatus && content === "" ? <p style={{ color: "red" }}>Hãy nhập nội dung phim</p> : ""}
                            </div>

                        </form>

                        <div className="form-group text-right m-b-0">
                            <button className="btn btn-primary waves-effect waves-light" onClick={e => {
                                return this.props.data._id ?
                                    updateFilm() :
                                    addFilm()
                            }}>
                                Submit
                            </button>
                            <button className="btn btn-secondary waves-effect waves-light m-l-5" onClick={e => {
                                this.props.actions.handleIsTableFilm(true)
                            }}>
                                Cancel
                            </button>
                        </div>
                        <LoadingDialog open={this.state.openLoadingDialog} />
                        <MessageDialog message="Save success" handleDialog={this.props.actions.handleDialogMessage} open={this.props.openMessageDialog} />
                    </div>
                </div>

            </div >
        )
    }
}
