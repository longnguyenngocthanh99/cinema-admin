export const GET_MOVIES = "FILMPAGE/GET_MOVIES"
export const GET_MOVIES_SUCCESS = "FILMPAGE/GET_MOVIES_SUCCESS"
export const GET_MOVIES_FAIL = "FILMPAGE/GET_MOVIES_FAIL"

export const GET_REVENUE = "FILMPAGE/GET_REVENUE"
export const GET_REVENUE_SUCCESS = "FILMPAGE/GET_REVENUE_SUCCESS"
export const GET_REVENUE_FAIL = "FILMPAGE/GET_REVENUE_FAIL"

export const ADD_FILM = "FILMPAGE/ADD_FILM"
export const ADD_FILM_SUCCESS = "FILMPAGE/ADD_FILM_SUCCESS"
export const ADD_FILM_FAIL = "FILMPAGE/ADD_FILM_FAIL"

export const UPDATE_FILM = "FILMPAGE/UPDATE_FILM"
export const UPDATE_FILM_SUCCESS = "FILMPAGE/UPDATE_FILM_SUCCESS"
export const UPDATE_FILM_FAIL = "FILMPAGE/UPDATE_FILM_FAIL"

export const GET_FILE = "FILMPAGE/GET_FILE"
export const HANDLE_IS_UPDATE = "FILMPAGE/HANDLE_IS_UPDATE"
export const HANDLE_LOAD_UPDATE = "FILMPAGE/HANDLE_LOAD_UPDATE"


export const HANDLE_INIT_DATA = "FILMPAGE/HANDLE_INIT_DATA"
export const HANDLE_DIALOG_MESSAGE = "FILMPAGE/HANDLE_DIALOG_MESSAGE"
export const HANDLECHANGETEXT = "FILMPAGE/HANDLECHANGETEXT"
export const HANDLECHANGEDATE = "FILMPAGE/HANDLECHANGEDATE"
export const HANDLE_IS_TABLE_FILM = "FILMPAGE/HANDLE_IS_TABLE_FILM"
export const HANDLE_GET_INFO_FILM = "FILMPAGE/HANDLE_GET_INFO_FILM"
export const HANDLE_FORM_STATUS = "FILMPAGE/HANDLE_FORM_STATUS"