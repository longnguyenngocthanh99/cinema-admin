import freeze from "deep-freeze"
import { handleActions } from "redux-actions"
import * as actions from "./actions"

export const name = "FilmPage"

const intialState = freeze({
    movies: [],
    data: {
        time: "",
        content: "",
        name: "",
        country: "",
        director: "",
        status: "avaiable",
        trailer: "",
        img1: "",
        img2: "",
        dayShow: "",
    },
    dateForm: new Date,
    imgShow1: "",
    imgShow2: "",
    isTableFilm: true,
    openLoadingDialog: false,
    openMessageDialog: false,
    formStatus: true
})

export default handleActions({
    [actions.handleFormStatus]: (state, action) => {
        return freeze({
            ...state,
            formStatus: action.payload
        })
    },
    [actions.handleChangeText]: (state, action) => {
        return freeze({
            ...state,
            data: {
                ...state.data,
                [action.payload.name]: action.payload.value
            }
        })
    },
    [actions.handleDialogMessage]: (state, action) => {
        return freeze({
            ...state,
            openMessageDialog: action.payload
        })
    },
    [actions.handleChangeDate]: (state, action) => {
        console.log(action.payload)
        const date = action.payload
        return freeze({
            ...state,
            data: {
                ...state.data,
                dayShow: date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear()
            },
            dateForm: date
        })
    },
    [actions.getFile]: (state, action) => {
        const data = action.payload.files[0]
        console.log(data)
        if (action.payload.name === "img1")
            return freeze({
                ...state,
                data: {
                    ...state.data,
                    img1: data
                },
                imgShow1: URL.createObjectURL(data)
            });
        return freeze({
            ...state,
            data: {
                ...state.data,
                img2: data
            },
            imgShow2: URL.createObjectURL(data)
        });
    },
    [actions.getMoviesSuccess]: (state, action) => {
        return freeze({
            ...state,
            movies: action.payload.movies
        })
    },
    [actions.updateFilmSuccess]: (state, action) => {
        return freeze({
            ...state,
            data: { ...action.payload },
            openLoadingDialog: false,
            openMessageDialog: true
        })
    },
    [actions.addFilmSuccess]: (state, action) => {
        return freeze({
            ...state,
            openLoadingDialog: false,
            openMessageDialog: true
        })
    },
    [actions.handleIsTableFilm]: (state, action) => {
        return freeze({
            ...state,
            isTableFilm: action.payload
        })
    },
    [actions.handleInitData]: (state, action) => {
        return freeze({
            ...state,
            data: intialState.data,
            imgShow1: "",
            imgShow2: ""
        })
    },
    [actions.handleGetInfoFilm]: (state, action) => {
        return freeze({
            ...state,
            data: {
                ...state.data,
                ...action.payload,
                img1: action.payload.img1,
                img2: action.payload.img2,
            },
            dateForm: new Date(action.payload.dayShow.split("/").reverse().join("-")),
            imgShow1: "http://localhost:8080/api/h/film/img/" + action.payload.img1,
            imgShow2: "http://localhost:8080/api/h/film/img/" + action.payload.img2,
        })
    },
},
    intialState
)