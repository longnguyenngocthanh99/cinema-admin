import { values } from "lodash/object"
import { fork, all } from "redux-saga/effects"
import { flatten } from "lodash/array"

import { sagas as LoginPage } from "./modules/LoginPage"
import { sagas as FilmPage } from "./modules/FilmPage"
import { sagas as CinemaPage } from "./modules/CinemaPage"
import { sagas as MemberPage } from "./modules/MemberPage"
import { sagas as ShowtimePage } from "./modules/ShowtimePage"
import { sagas as HomePage } from "./modules/HomePage"

const sagasList = [
    LoginPage,
    FilmPage,
    CinemaPage,
    MemberPage,
    ShowtimePage,
    HomePage,
]

export default function* () {
    yield all(flatten(sagasList.map(sagas => values(sagas))).map(saga => fork(saga)))
}