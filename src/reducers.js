import { combineReducers } from "redux"
import { connectRouter } from "connected-react-router"

import LoginPage, { name as nameOfLoginPage } from "./modules/LoginPage"
import FilmPage, { name as nameOfFilmPage } from "./modules/FilmPage"
import CinemaPage, { name as nameOfCinemaPage } from "./modules/CinemaPage"
import MemberPage, { name as nameOfMemberPage } from "./modules/MemberPage"
import ShowtimePage, { name as nameOfShowtimePage } from "./modules/ShowtimePage"
import HomePage, { name as nameOfHomePage } from "./modules/HomePage"

const reducers = {
    [nameOfLoginPage]: LoginPage,
    [nameOfFilmPage]: FilmPage,
    [nameOfCinemaPage]: CinemaPage,
    [nameOfMemberPage]: MemberPage,
    [nameOfHomePage]: HomePage,
    [nameOfShowtimePage]: ShowtimePage,
}

export default (history) => combineReducers({
    ...reducers,
    router: connectRouter(history)
})