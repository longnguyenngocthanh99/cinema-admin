import {createStore, applyMiddleware, compose} from "redux"
import {createLogger} from "redux-logger"
import createSagaMiddleware, {END} from "redux-saga"
import {routerMiddleware, connectRouter} from "connected-react-router"
import {createBrowserHistory, createMemoryHistory} from "history"
import makeRootReducer from "./reducers";
import sagas from "./sagas";

export const isServer = !(
    typeof window !== "undefined" && 
    window.document &&
    window.document.createElement
)

export const history = isServer 
    ? createMemoryHistory({
        initialEntries : ["/"]
    }) :
    createBrowserHistory()

export default () =>{
    const activeEffectIds = []
    const watchEffectEnd = (effectId)=>{
        const effectIndex = activeEffectIds.indexOf(effectId)

        if(effectIndex !== -1){
            activeEffectIds.splice(effectIndex, 1)
        }
    }

    const sagaMiddleWare = createSagaMiddleware({
        sagaMonitor :{
            effectCancelled :watchEffectEnd,
            effectRejected :watchEffectEnd,
            effectResolved : watchEffectEnd,
            effectTriggered : (event) =>{
                if(event.effect.FORK){
                    activeEffectIds.push(event.effectId)
                }
            }
        }
    })

    const middleware = routerMiddleware(history)
    const loggerMiddleware = createLogger({
        predicate : () => process.env.NODE_ENV === "development"
    })

    const initialstate = !isServer ? window.__PRELOADED_STATE__ : {}
    
    if(!isServer){
        delete window.__PRELOADED_STATE__
    }

    const store = createStore(
        connectRouter(history)(makeRootReducer(history)),
        initialstate,
        compose(applyMiddleware(sagaMiddleWare, middleware, loggerMiddleware))
    )

    if(module.hot){
        module.hot.accept("./reducers",()=>{
            const nextReducer = require("./reducers")
            store.replaceReducer(nextReducer)
        })
    }
    store.runSaga = sagaMiddleWare.run(sagas)
    store.close = ()=>store.dispatch(END)
    store.activeEffectIds = activeEffectIds

    return{
        store,
        history
    }
}
