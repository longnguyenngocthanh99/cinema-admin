import React, { Component } from "react";
import { Provider } from "react-redux"
import { ConnectedRouter } from "connected-react-router"
import { Route, Switch } from "react-router-dom"

import LoginPage from "./modules/LoginPage/components/LoginPageContainer"
import HomePage from "./modules/HomePage/components/HomePageContainer"
import FilmPage from "./modules/FilmPage/components/FilmPageContainer"
import CinemaPage from "./modules/CinemaPage/components/CinemaPageContainer"
import WrappedMenu from "./modules/WrappedMenu/components/WrappedMenuContainer"
import MemberPage from "./modules/MemberPage/components/MemberPageContainer"
import ShowtimePage from "./modules/ShowtimePage/components/ShowtimePageContainer"

import configureStore from "./stores"
const { store, history } = configureStore()

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <Switch>
            <WrappedMenu>
              <Route path="/" exact component={HomePage} />
              <Route path="/film" exact component={FilmPage} />
              <Route path="/cinema" exact component={CinemaPage} />
              <Route path="/member" exact component={MemberPage} />
              <Route path="/showtime" exact component={ShowtimePage} />
            </WrappedMenu>
          </Switch>
        </ConnectedRouter>
      </Provider>
    );
  }
}

export default App;
