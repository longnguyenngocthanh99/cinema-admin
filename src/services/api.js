const urlCinema = "http://localhost:8080/api/h/cinema/"

const urlFilm = "http://localhost:8080/api/quest/film/"

const urlFilmUpdate = "http://localhost:8080/api/h/film/"

const urlTheater = "http://localhost:8080/api/h/theater/"

const urlTheaterByCinema = "http://localhost:8080/api/h/theater/cinema/"

const urlShowtimes = "http://localhost:8080/api/h/showtime/"

const urlShowtimesInit = "http://localhost:8080/api/h/showtime/init"

const urlShowtimesGeneral = "http://localhost:8080/api/h/showtime/general"

const urlLogin = "http://localhost:8080/api/user/login"

const urlQuestRegister = "http://localhost:8080/api/quest/members"

const urlMember = "http://localhost:8080/api/h/member/"

const urlUpdateInfoMember = "http://localhost:8080/api/h/member"

const urlTicketsInfoMember = "http://localhost:8080/api/h/member/ticket"

const urlGetRevenueFilm = "http://localhost:8080/api/h/showtime/revenue_film"

const urlGetRevenueTickets = "http://localhost:8080/api/h/showtime/revenue_ticket/"

const urlGetRevenueCombo = "http://localhost:8080/api/h/showtime/revenue_combo/"

const urlImg = "http://localhost:8080/api/h/film/img/"

export default {
    urlCinema,
    urlFilm,
    urlShowtimes,
    urlShowtimesGeneral,
    urlShowtimesInit,
    urlGetRevenueFilm,
    urlGetRevenueCombo,
    urlGetRevenueTickets,
    urlLogin,
    urlQuestRegister,
    urlMember,
    urlUpdateInfoMember,
    urlTicketsInfoMember,
    urlImg,
    urlFilmUpdate,
    urlTheater,
    urlTheaterByCinema,
}